#ifndef MESHRRILL_IO_CACHEDTIFFREADER_HPP
#define MESHRRILL_IO_CACHEDTIFFREADER_HPP


#include "meshrrill/config.h"


#ifndef MESHRRILL_ENABLE_TIFF_READER
#error "CachedTIFFReader was not configured for this installation!"
#endif


#include <cstddef>
#include <cstdint>
#include <stdexcept>
#include <utility>
#include <vector>


struct tiff;

namespace meshrrill {


//
// Read a TIFF image, providing a std::uint32_t value(x,y,z) interface for
// reading values of the image at (x,y) on layer z, along with
// caching for the last 3 unique layer reads. This should minimize image
// reads while using ImageToPolyhedron.
//
class CachedTIFFReader {
public:
    std::uint32_t nx;
    std::uint32_t ny;
    std::uint32_t nz;

    using FT = double;
    const FT vx;
    const FT vy;
    const FT vz;

    ::tiff* tiff;

    using Raster      = std::vector<std::uint32_t>;
    using RasterCache = std::vector<std::pair<std::size_t, Raster>>;
    RasterCache raster_cache;

    struct Exception : public std::runtime_error {
        explicit Exception(const std::string& what_arg) :
            std::runtime_error(what_arg) {}
    };


    CachedTIFFReader(FT vx, FT vy, FT vz);

    ~CachedTIFFReader();

    CachedTIFFReader()                        = delete;
    CachedTIFFReader(const CachedTIFFReader&) = delete;
    CachedTIFFReader(CachedTIFFReader&&)      = delete;
    CachedTIFFReader& operator=(const CachedTIFFReader&) = delete;
    CachedTIFFReader& operator=(CachedTIFFReader&&) = delete;


    void read(std::istream& istream);

    void read_layer(std::size_t z, Raster& raster);
    const Raster& get_cached_layer(std::size_t z);

    std::uint32_t value(std::size_t x, std::size_t y, std::size_t z);
};

} // namespace meshrrill

#endif // MESHRRILL_IO_CACHEDTIFFREADER_HPP
