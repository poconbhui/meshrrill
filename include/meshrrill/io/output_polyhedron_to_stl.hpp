#ifndef MESHRRILL_IO_OUTPUT_POLYHEDRON_TO_STL_HPP
#define MESHRRILL_IO_OUTPUT_POLYHEDRON_TO_STL_HPP


#include <exception>

template<typename Stream, typename Polyhedron>
void output_polyhedron_to_stl(
    Stream& polyhedron_stl, const Polyhedron& polyhedron) {
    using PFit    = typename Polyhedron::Facet_const_iterator;
    using PHc     = typename Polyhedron::Halfedge_around_facet_const_circulator;
    using Point_3 = typename Polyhedron::Point_3;

    // The polyhedron must be triangular!
    // if(!polyhedron.is_pure_triangle()) {
    //    throw std::logic_error(
    //        "output_polyhedron_stl: Input polyhedron must be pure triangular."
    //    );
    //}

    polyhedron_stl << "solid particle\n";
    for(PFit pfit = polyhedron.facets_begin(); pfit != polyhedron.facets_end();
        pfit++) {
        PHc phc     = pfit->facet_begin();
        PHc phc_end = phc;

        polyhedron_stl << "facet normal 0 0 0\n"
                       << "   outer loop\n";
        do {
            const Point_3& p = phc->vertex()->point();

            polyhedron_stl << "        vertex " << p.x() << " " << p.y() << " "
                           << p.z() << "\n";

            phc++;
        } while(phc != phc_end);
        polyhedron_stl << "   endloop\n"
                       << "endfacet\n";
    }
    polyhedron_stl << "endsolid particle"
                   << "\n";
}


#endif // MESHRRILL_IO_OUTPUT_POLYHEDRON_TO_STL_HPP
