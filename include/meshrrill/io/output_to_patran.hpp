#ifndef MESHRRILL_IO_OUTPUT_TO_PATRAN_HPP
#define MESHRRILL_IO_OUTPUT_TO_PATRAN_HPP

#include "meshrrill/geometry/MeshIterator.hpp"

#include <CGAL/Kernel/global_functions.h>
#include <CGAL/number_utils.h>

#include <algorithm>
#include <cstdio>
#include <exception>
#include <map>
#include <sstream>
#include <vector>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// Output Patran file                                                        //
///////////////////////////////////////////////////////////////////////////////
//
// Output meshes to a Patran file, suitable for use in MERRILL.
//

namespace internal {
class UniformCellIdsIterator {
public:
    std::size_t cell_id;

    explicit UniformCellIdsIterator(std::size_t cell_id = 1) :
        cell_id(cell_id) {}

    std::size_t operator*() {
        return cell_id;
    }

    UniformCellIdsIterator operator++() {
        return *this;
    }
    UniformCellIdsIterator& operator++(int) {
        return *this;
    }
    bool operator==(const UniformCellIdsIterator& /*unused*/) {
        return false;
    }
    bool operator!=(const UniformCellIdsIterator& /*unused*/) {
        return true;
    }
};

//
// Use snprintf to write the formatted args to the stream.
//
template<typename Stream, typename... Args>
void printf_to_stream(Stream& stream, const std::string& format, Args... args) {
    // Use snprintf to generate the format string into a temporary
    // buffer, then print the buffer out to the stream.

    // Buffer more than large enough for a patran file line.
    // The buffer is intentionally mostly uninitialized, so NOLINT it.
    const int n_strbuf = 1024;
    std::array<char, n_strbuf> strbuf; // NOLINT
    strbuf[0] = '\0';

    // Using snprintf raises a warning in clang-tidy under
    // cppcoreguidelines-pro-type-vararg.
    // We know what we're doing here (!), so NOLINT it.
    [[maybe_unused]] int size = std::snprintf( // NOLINT
        strbuf.data(), n_strbuf, format.c_str(), args...);
    assert(size >= 0 && "Error writing to buffer.");
    assert(size < n_strbuf && "Formatted string larger than buffer.");

    // Print the buffer to the stream.
    stream << strbuf.data();
}
} // namespace internal


//
// Generate Patran file from a list of points and connectivities
//

template<
    typename Stream, typename PointIterator, typename ConnectivityIterator,
    typename CellIdIterator>
void output_to_patran(
    Stream& pat_file, PointIterator points_begin,
    const PointIterator& points_end, ConnectivityIterator connectivities_begin,
    const ConnectivityIterator& connectivities_end,
    CellIdIterator cell_ids_begin, const CellIdIterator& cell_ids_end) {

    // Common string format for Patran neutral card headers
    std::string i28i8 = "%02d%8d%8d%8d%8d%8d%8d%8d%8d\n";


    ///////////////////////////////////////////////////////////////////////////
    // Start Patran file.                                                    //
    ///////////////////////////////////////////////////////////////////////////
    // File format taken from                                                //
    // http://www.ansys.com/staticassets/ANSYS                               //
    //  /Initial%20Content%20Entry/General%20Articles%20-%20Products         //
    //  /ICEM%20CFD%20Interfaces/patran.htm                                  //
    //                                                                       //
    // and (more completely, especially for format strings)                  //
    //     Patran 2014.1 Reference Manual Part 1: Basic Functions,           //
    //     The Neutral File, p 892                                           //
    // found at                                                              //
    //     https://simcompanion.mscsoftware.com/infocenter/                  //
    //         index?page=content&id=DOC10756                                //
    //         &cat=2014.1_PATRAN_DOCS&actp=LIST                             //
    ///////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////
    // Packet 25: Title Card                                                 //
    ///////////////////////////////////////////////////////////////////////////
    // The title card packet contains the following:                         //
    //         25 ID IV KC                                                   //
    //         TITLE                                                         //
    //                                                                       //
    // where:  ID = 0 (not applicable)                                       //
    //         IV = 0 (not applicable                                        //
    //         KC = 1                                                        //
    //         TITLE = ICEM-PATRAN INTERFACE - Version ... -                 //
    //                                                                       //
    // format: (I2, 8I8)                                                     //
    //         (80A4)                                                        //
    ///////////////////////////////////////////////////////////////////////////

    // 25 ID IV KC
    // (I2, 8I8)
    internal::printf_to_stream(pat_file, i28i8, 25, 0, 0, 1, 0, 0, 0, 0, 0);

    // TITLE
    // (80A4)
    internal::printf_to_stream(
        pat_file, "%.320s\n",
        "Mesh generator by Padraig O Conbhui using the CGAL library.");


    ///////////////////////////////////////////////////////////////////////////
    // Packet 26: Summary Data                                               //
    ///////////////////////////////////////////////////////////////////////////
    // The summary data packet contains the following:                       //
    //         26 ID IV KC N1 N2 N3 N4 N5                                    //
    //         DATE TIME VERSION                                             //
    //                                                                       //
    // where:  ID = 0 (not applicable)                                       //
    //         IV = 0 (not applicable)                                       //
    //         KC = 1                                                        //
    //         N1 = number of nodes                                          //
    //         N2 = number of elements                                       //
    //         N3 = number of materials                                      //
    //         N4 = number of element properties                             //
    //         N5 = number of coordinate frames                              //
    //         DATE = dd-mm-yy                                               //
    //         TIME = hh:mm:ss                                               //
    //         VERSION = 2.5                                                 //
    //                                                                       //
    // format: (I2, 8I8)                                                     //
    //         (3A4, 2A4, 3A4)                                               //
    ///////////////////////////////////////////////////////////////////////////

    // TODO(paddy): Add number of materials (ie number of subdomains)

    // 26 ID IV KC N1 N2 N3 N4 N5
    // (I2, 8I8)
    internal::printf_to_stream(
        pat_file, i28i8, 26, 0, 0, 1, std::distance(points_begin, points_end),
        std::distance(connectivities_begin, connectivities_end), 1, 0, 0);

    // DATE TIME VERSION
    // (3A4, 2A4, 3A4)
    internal::printf_to_stream(
        pat_file, "%-12s%-8s%12s\n", "00/00/00", "00:00:00", "2.5");


    ///////////////////////////////////////////////////////////////////////
    // Packet 01: Node Data                                              //
    ///////////////////////////////////////////////////////////////////////
    // The node data packet contains the following:                      //
    //         1 ID IV KC                                                //
    //         X Y Z                                                     //
    //         ICF GTYPE NDF CONFIG CID PSP                              //
    //                                                                   //
    // where:  ID = node ID                                              //
    //         IV = 0 (not applicable)                                   //
    //         KC = number of lines in data card = 2                     //
    //         X, Y, Z = X, Y, Z cartesian coordinate of the node        //
    //         ICF = 0 (unreferenced)                                    //
    //             Not sure about this one, but CUBIT uses 0, so ...     //
    //         GTYPE = G                                                 //
    //         NDF = 2 or 3 for 2D or 3D model respectively              //
    //         CONFIG = 0 (not applicable)                               //
    //         CID = 0 i.e. global cartesian coordinate system           //
    //         PSPC = 000000 (not used)                                  //
    //                                                                   //
    // format: (I2, 8I8)                                                 //
    //         (3E16.9) (Maybe actually (E15.8, 1X, E15.8, 1X, E15.8)    //
    //         (I1, 1A1, I8, I8, I8, 2X, 6I1)                            //
    ///////////////////////////////////////////////////////////////////////

    std::size_t current_vertex_index = 1;
    for(PointIterator pit = points_begin; pit != points_end;
        pit++, current_vertex_index++) {
        typename PointIterator::reference p = *pit;

        // 1 ID IV KC
        // (I2, 8I8)
        internal::printf_to_stream(
            pat_file, i28i8, 1, current_vertex_index, 0, 2, 0, 0, 0, 0, 0);

        // X Y Z
        // (3E16.9) (Maybe actually (E15.8, 1X, E15.8, 1X, E15.8)
        internal::printf_to_stream(
            pat_file, " % 15.8E % 15.8E % 15.8E\n", CGAL::to_double(p.x()),
            CGAL::to_double(p.y()), CGAL::to_double(p.z()));


        // ICF GTYPE NDF CONFIG CID PSP
        // (I1, 1A1, I8, I8, I8, 2X, 6I1)
        internal::printf_to_stream(
            pat_file, "%1d%1c%8d%8d%8d  %1d%1d%1d%1d%1d%1d\n", 0, 'G', 3, 0, 0,
            0, 0, 0, 0, 0, 0);
    }


    ///////////////////////////////////////////////////////////////////////
    // Packet 02: Element Data                                           //
    ///////////////////////////////////////////////////////////////////////
    // The element data packet contains the following:                   //
    //         2 ID IV KC N1 N2                                          //
    //         NODES CONFIG PID CEID q1 q2 q3                            //
    //         LNODES                                                    //
    //         ADATA                                                     //
    //                                                                   //
    // where:  ID = element ID                                           //
    //         IV = shape (2=bar, 3=tri, 4=quad,                         //
    //                     5=tet, 7=wedge, 8=hex, 9=pyra)                //
    //         KC = number of lines in data card                         //
    //         N1 = 0 (not used)                                         //
    //         N2 = 0 (not used)                                         //
    //         NODES = number of nodes in the element                    //
    //         CONFIG = 0 (not used)                                     //
    //         PID = element property ID                                 //
    //         CEID = 0 (not used)                                       //
    //         q1,q2,q3 = 0 (not used)                                   //
    //         LNODES = element corner nodes followed by                 //
    //                  additional nodes                                 //
    //         ADATA : not used                                          //
    //                                                                   //
    // format: (I2, 8I8)                                                 //
    //         (I8, I8, I8, I8, 3E16.9)                                  //
    //         (10I8)                                                    //
    ///////////////////////////////////////////////////////////////////////

    // We want to ensure the signed cell volumes we generate are all positive.
    // We also want them as close to ascending order as we can get.

    std::size_t current_cell_index = 1;
    CellIdIterator cid_it          = cell_ids_begin;
    for(ConnectivityIterator cit = connectivities_begin;
        cit != connectivities_end && cid_it != cell_ids_end;
        cit++, cid_it++, current_cell_index++) {

        // 2 ID IV KC N1 N2
        // (I2, 8I8)
        internal::printf_to_stream(
            pat_file, i28i8, 2, current_cell_index, 5, 2, 0, 0, 0, 0, 0);

        // NODES CONFIG PID CEID q1 q2 q3
        // (I8, I8, I8, I8, 3E16.9)
        internal::printf_to_stream(
            pat_file, "%8d%8d%8lu%8d % 15.8E % 15.8E % 15.8E\n", 4, 0,
            static_cast<uint64_t>(*cid_it) /*Domain Index*/, 0, 0.0, 0.0, 0.0);

        // LNODES
        // (10I8) (well, 4I8 here)
        internal::printf_to_stream(
            pat_file, "%8lu%8lu%8lu%8lu\n",
            // + 1 because CGAL is 0 indexed, but Patran is 1 indexed.
            static_cast<uint64_t>((*cit)[0] + 1),
            static_cast<uint64_t>((*cit)[1] + 1),
            static_cast<uint64_t>((*cit)[2] + 1),
            static_cast<uint64_t>((*cit)[3] + 1));
    }

    // Packet 99: End of Neutral File
    internal::printf_to_stream(pat_file, i28i8, 99, 0, 0, 1, 0, 0, 0, 0, 0);

    // Done outputting mesh ///////////////////////////////////////////////////
}

template<typename Stream, typename PointIterator, typename ConnectivityIterator>
void output_to_patran(
    Stream& pat_file, PointIterator points_begin,
    const PointIterator& points_end, ConnectivityIterator connectivities_begin,
    const ConnectivityIterator& connectivities_end) {
    internal::UniformCellIdsIterator ucid_it(1);
    output_to_patran(
        pat_file, points_begin, points_end, connectivities_begin,
        connectivities_end, ucid_it, ucid_it);
}


//
// Generate a Patran file from a Mesh. Should also work with a CGAL::C3t3 type.
//
template<typename Stream, typename Mesh>
void output_mesh_to_patran(Stream& pat_file, const Mesh& mesh) {
    meshrrill::MeshIterator<Mesh> mesh_iterator(mesh);

    using CCit = typename Mesh::Cells_in_complex_iterator;
    std::vector<std::size_t> cell_ids;
    for(CCit ccit = mesh.cells_in_complex_begin();
        ccit != mesh.cells_in_complex_end(); ccit++) {
        cell_ids.push_back(mesh.subdomain_index(ccit));
    }

    output_to_patran(
        pat_file, mesh_iterator.points_begin(), mesh_iterator.points_end(),
        mesh_iterator.connectivities_begin(),
        mesh_iterator.connectivities_end(), cell_ids.begin(), cell_ids.end());
}


} // namespace meshrrill


#endif // MESHRRILL_IO_OUTPUT_TO_PATRAN_HPP
