#ifndef MESHRRILL_CGAL_HACKS_STD_SQRT_SPECIALIZATIONS_HPP
#define MESHRRILL_CGAL_HACKS_STD_SQRT_SPECIALIZATIONS_HPP


//
// Some functions in CGAL use std::sqrt instead of CGAL::Sqrt.
// This file defines some template specializations of std::sqrt which
// simply call CGAL::Sqrt for some types.
//


// Set this in defines to disable this hack.
// Shouldn't be a problem in CGAL-4.8 ... ?
#ifndef MESHRRILL_NO_EPECK_STD_SQRT_SPECIALIZATION

#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <cmath>


// Add template specialization of std::sqrt
// for Filtered_kernel<Epeck_with_sqrt> using the built in CGAL::sqrt
// definition.
namespace meshrrill {

typedef CGAL::Filtered_kernel<
    CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt>
    Filtered_Epeck_with_sqrt;
typedef CGAL::Algebraic_structure_traits<Filtered_Epeck_with_sqrt::FT>
    Filtered_Epeck_with_sqrt_AST;

} // namespace meshrrill

namespace std {

meshrrill::Filtered_Epeck_with_sqrt_AST::Sqrt::result_type
sqrt(meshrrill::Filtered_Epeck_with_sqrt_AST::Sqrt::argument_type x) {
    return CGAL::sqrt(x);
}


} // namespace std


#endif // MESHRRILL_NO_EPECK_STD_SQRT_SPECIALIZATION


#endif // MESHRRILL_CGAL_HACKS_STD_SQRT_SPECIALIZATIONS_HPP
