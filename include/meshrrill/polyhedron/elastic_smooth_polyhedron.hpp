#ifndef MESHRRILL_POLYHEDRON_ELASTIC_SMOOTH_POLYHEDRON_HPP
#define MESHRRILL_POLYHEDRON_ELASTIC_SMOOTH_POLYHEDRON_HPP

#include <map>
#include <vector>

#include <CGAL/Origin.h>

namespace meshrrill {

//
// Smooth polyhedron, allowing points to move within a vx x vy x vz box
// while trying to minimize the curvature of the surface.
//
template<typename Polyhedron, typename FT>
void elastic_smooth_polyhedron(
    Polyhedron& polyhedron, const FT vx, const FT vy, const FT vz) {
    using Kernel = typename Polyhedron::Traits;
    using Point  = typename Kernel::Point_3;
    using Vector = typename Kernel::Vector_3;

    using Vertex_const_handle   = typename Polyhedron::Vertex_const_handle;
    using Vertex_iterator       = typename Polyhedron::Vertex_iterator;
    using Vertex_const_iterator = typename Polyhedron::Vertex_const_iterator;
    using Halfedge_around_vertex_const_circulator =
        typename Polyhedron::Halfedge_around_vertex_const_circulator;

    using VertexIdMap  = std::map<Vertex_const_handle, std::size_t>;
    using NeighbourIds = std::vector<std::vector<std::size_t>>;
    using Forces       = std::vector<Vector>;
    using Positions    = std::vector<Point>;


    VertexIdMap vertex_id_map;

    const std::size_t n_polyhedron_vertices = polyhedron.size_of_vertices();

    std::vector<Point> original_positions(n_polyhedron_vertices);
    Positions positions(n_polyhedron_vertices);
    Forces forces(n_polyhedron_vertices);
    Forces neighbour_forces(n_polyhedron_vertices);
    NeighbourIds neighbour_ids(n_polyhedron_vertices);

    // Populate vertex_id_map and index based on vertex mapping order.
    for(Vertex_const_iterator vit = polyhedron.vertices_begin();
        vit != polyhedron.vertices_end(); vit++) {
        vertex_id_map[vit] = 0;
    }

    std::size_t vertex_id = 0;
    for(auto vid_it = vertex_id_map.begin(); vid_it != vertex_id_map.end();
        vid_it++, vertex_id++) {
        vid_it->second = vertex_id;
    }


    // Populate original_positions, positions and neighbour_ids
    for(typename VertexIdMap::const_iterator vid_it = vertex_id_map.begin();
        vid_it != vertex_id_map.end(); vid_it++) {
        const Vertex_const_handle vertex_handle = vid_it->first;
        const std::size_t vertex_id             = vid_it->second;

        original_positions[vertex_id] = vertex_handle->point();
        positions[vertex_id]          = vertex_handle->point();

        Halfedge_around_vertex_const_circulator hc =
            vertex_handle->vertex_begin();
        Halfedge_around_vertex_const_circulator hc_end = hc;

        // Get immediate neighbours
        do {
            const Vertex_const_handle v = hc->opposite()->vertex();
            // neighbour_set.insert(v);
            neighbour_ids[vertex_id].push_back(vertex_id_map[v]);
            hc++;
        } while(hc != hc_end);
    }


    for(std::size_t i = 0; i < 1000; i++) {
        // Generate force terms
        std::fill(forces.begin(), forces.end(), CGAL::NULL_VECTOR);
        std::fill(
            neighbour_forces.begin(), neighbour_forces.end(),
            CGAL::NULL_VECTOR);

        for(std::size_t vertex_id = 0; vertex_id < n_polyhedron_vertices;
            vertex_id++) {
            typename NeighbourIds::const_reference neighbour_ids_i =
                neighbour_ids[vertex_id];

            // Determine force on this vertex due to position of other vertices
            Point neighbour_center = CGAL::ORIGIN;
            for(auto nid_it : neighbour_ids_i) {
                neighbour_center =
                    neighbour_center + (positions[nid_it] - CGAL::ORIGIN);
            }
            neighbour_center = neighbour_center.transform(
                {CGAL::Scaling(), 1, static_cast<FT>(neighbour_ids_i.size())});

            const Vector force = (neighbour_center - positions[vertex_id]);

            forces[vertex_id] = force; // forces[vertex_id] + force;

            // Add force on this vertex onto the other vertices
            for(auto nid_it : neighbour_ids_i) {
                neighbour_forces[nid_it] = neighbour_forces[nid_it] - force;
            }
        }

        // Update positions
        for(std::size_t vertex_id = 0; vertex_id < n_polyhedron_vertices;
            vertex_id++) {
            const FT local_coupling     = 0.25;
            const FT neighbour_coupling = 0.25;

            Point& point = positions[vertex_id];
            point        = point + local_coupling * forces[vertex_id]
                    + neighbour_coupling * neighbour_forces[vertex_id]
                          / neighbour_ids[vertex_id].size();

            const Point original_position = original_positions[vertex_id];

            const FT xmax = original_position.x() + vx / 2;
            const FT xmin = original_position.x() - vx / 2;
            const FT ymax = original_position.y() + vy / 2;
            const FT ymin = original_position.y() - vy / 2;
            const FT zmax = original_position.z() + vz / 2;
            const FT zmin = original_position.z() - vz / 2;

            if(point.x() > xmax) {
                point = Point(xmax, point.y(), point.z());
            }
            if(point.x() < xmin) {
                point = Point(xmin, point.y(), point.z());
            }
            if(point.y() > ymax) {
                point = Point(point.x(), ymax, point.z());
            }
            if(point.y() < ymin) {
                point = Point(point.x(), ymin, point.z());
            }
            if(point.z() > zmax) {
                point = Point(point.x(), point.y(), zmax);
            }
            if(point.z() < zmin) {
                point = Point(point.x(), point.y(), zmin);
            }
        }
    }

    for(Vertex_iterator vit = polyhedron.vertices_begin();
        vit != polyhedron.vertices_end(); vit++) {
        vit->point() = positions[vertex_id_map[vit]];
    }
}

} // namespace meshrrill

#endif // MESHRRILL_POLYHEDRON_ELASTIC_SMOOTH_POLYHEDRON_HPP
