#ifndef MESHRRILL_POLYHEDRON_POLYHEDRONCONVERTER_HPP
#define MESHRRILL_POLYHEDRON_POLYHEDRONCONVERTER_HPP


#include <CGAL/Cartesian_converter.h>
#include <CGAL/Modifier_base.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>

#include <map>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// PolyhedronConverter                                                       //
///////////////////////////////////////////////////////////////////////////////
//
// A class which converts CGAL::Polyhedron_3 between kernel types.
//
// This is useful for converting polyhedra between exact and inexact kernels.
//
template<typename Poly1, typename Poly2>
class PolyhedronConverter
    : public CGAL::Modifier_base<typename Poly2::HalfedgeDS> {
public:
    const Poly1& poly1;

    explicit PolyhedronConverter(const Poly1& poly1) : poly1(poly1) {}

    using Kernel1 = typename Poly1::Traits;
    using HDS1    = typename Poly1::HalfedgeDS;
    using Vertex1 = typename HDS1::Vertex;
    using Point1  = typename Vertex1::Point;

    using Kernel2 = typename Poly2::Traits;
    using HDS2    = typename Poly2::HalfedgeDS;
    using Vertex2 = typename HDS2::Vertex;
    using Point2  = typename Vertex2::Point;

    using K1_to_K2 = CGAL::Cartesian_converter<Kernel1, Kernel2>;


    void operator()(HDS2& hds2) override {
        // Use a map to refer to vertex handles by 0-indexed id later.
        using Vertex_handle1  = typename Poly1::Vertex_const_handle;
        using VertexHandleMap = std::map<Vertex_handle1, int>;
        VertexHandleMap vertex_handle_to_id;

        // Use a Cartesian_converter to map between Point1 and Point2 types.
        K1_to_K2 k1_to_k2{};


        // Use an incremental builder to generate Poly2 from Poly1
        CGAL::Polyhedron_incremental_builder_3<HDS2> B2(hds2, true);
        B2.begin_surface(
            poly1.size_of_vertices(), poly1.size_of_facets(),
            poly1.size_of_halfedges());


        // Iterate over vertices of Poly1 and add them to Poly2.
        using Vcit = typename Poly1::Vertex_const_iterator;
        typename VertexHandleMap::mapped_type vcit_i = 0;
        for(Vcit vcit = poly1.vertices_begin(); vcit != poly1.vertices_end();
            vcit++, vcit_i++) {
            vertex_handle_to_id[vcit] = vcit_i;

            Point1 p1 = vcit->point();
            Point2 p2 = k1_to_k2(p1);

            B2.add_vertex(p2);
        }

        // Iterate over the facets of Poly1 and add them to Poly2
        using Fcit = typename Poly1::Facet_const_iterator;
        for(Fcit fcit = poly1.facets_begin(); fcit != poly1.facets_end();
            fcit++) {
            B2.begin_facet();

            // Loop over the vertices in order to generate the facet.
            using HAFCc =
                typename Poly1::Halfedge_around_facet_const_circulator;
            HAFCc hafcc = fcit->facet_begin();
            do {
                B2.add_vertex_to_facet(vertex_handle_to_id[hafcc->vertex()]);
                hafcc++;
            } while(hafcc != fcit->facet_begin());

            B2.end_facet();
        }

        B2.end_surface();
    }
};


} // namespace meshrrill

#endif // MESHRRILL_POLYHEDRON_POLYHEDRONCONVERTER_HPP
