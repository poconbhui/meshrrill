#ifndef MESHRRILL_POLYHEDRON_POLYHEDRONMESHER_HPP
#define MESHRRILL_POLYHEDRON_POLYHEDRONMESHER_HPP

#include "meshrrill/geometry/ImplicitMesher.hpp"
#include "meshrrill/geometry/MeshDomainWithFeatures3Wrap.hpp"

#include <CGAL/Mesh_domain_with_polyline_features_3.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Side_of_triangle_mesh.h>

#include <set>
#include <vector>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// PolyhedronMesher                                                          //
///////////////////////////////////////////////////////////////////////////////
//
// This class defines the generic, basic bits needed to mesh a simple
// polyhedron, including steps for preserving sharp edges.
//
class PolyhedronMesher : public ImplicitMesher {
public:
    using Polyhedron = CGAL::Polyhedron_3<Kernel>;

    PolyhedronMesher(
        const Polyhedron& polyhedron, double cell_size,
        double detected_angle_bound = -1);
};


} // namespace meshrrill


#endif // MESHRRILL_POLYHEDRON_POLYHEDRONMESHER_HPP
