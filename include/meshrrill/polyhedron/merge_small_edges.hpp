#ifndef MESHRRILL_POLYHEDRON_MERGE_SMALL_EDGES_HPP
#define MESHRRILL_POLYHEDRON_MERGE_SMALL_EDGES_HPP


#include <CGAL/Kernel/global_functions_3.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_length_cost.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_length_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Midpoint_placement.h>
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>

namespace meshrrill {


template<typename Polyhedron>
void merge_small_edges(
    Polyhedron& polyhedron,
    const typename Polyhedron::Traits::FT min_edge_size) {

    namespace Sms = CGAL::Surface_mesh_simplification;

    Sms::edge_collapse(
        polyhedron, Sms::Edge_length_stop_predicate<double>(min_edge_size),
        CGAL::parameters::vertex_index_map(
            get(CGAL::vertex_external_index, polyhedron))
            .halfedge_index_map(get(CGAL::halfedge_external_index, polyhedron))
            .get_cost(Sms::Edge_length_cost<Polyhedron>())
            .get_placement(Sms::Midpoint_placement<Polyhedron>()));
}


} // namespace meshrrill

#endif // MESHRRILL_POLYHEDRON_MERGE_SMALL_EDGES_HPP
