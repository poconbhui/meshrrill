#ifndef MESHRRILL_POLYHEDRON_FLATTEN_SHARP_POINTS_HPP
#define MESHRRILL_POLYHEDRON_FLATTEN_SHARP_POINTS_HPP


#include <CGAL/Origin.h>

namespace meshrrill {

// Find vertices in the polyhedron where the sum of angles of incident edges
// is less than angle. For a perfectly flat surface, the total angle should
// be 2*pi. For a less than flat surface, it will be < 2*pi.
template<typename Polyhedron>
void flatten_sharp_points(
    Polyhedron& polyhedron, const typename Polyhedron::Traits::FT min_angle) {
    using Kernel = typename Polyhedron::Traits;
    using FT     = typename Kernel::FT;
    using Vector = typename Kernel::Vector_3;
    using Point  = typename Polyhedron::Point_3;

    using Vertex_iterator = typename Polyhedron::Vertex_iterator;
    using Halfedge_around_vertex_const_circulator =
        typename Polyhedron::Halfedge_around_vertex_const_circulator;

    for(Vertex_iterator vit = polyhedron.vertices_begin();
        vit != polyhedron.vertices_end(); vit++) {
        FT total_angle = FT();

        Halfedge_around_vertex_const_circulator hc     = vit->vertex_begin();
        Halfedge_around_vertex_const_circulator hc_end = hc;

        Point neighbour_center   = CGAL::ORIGIN;
        std::size_t n_neighbours = 0;

        Point& point = vit->point();
        do {
            Point p1 = hc->prev()->vertex()->point();
            Point p2 = hc->next()->vertex()->point();

            // Find the angle p1, point, p2
            const Vector a = p1 - point;
            const Vector b = p2 - point;

            const FT lab = CGAL::sqrt(a.squared_length() * b.squared_length());

            const FT th = std::acos(CGAL::to_double(a * b / lab));

            total_angle += th;

            // Add to neighbour_center
            neighbour_center = neighbour_center + Vector(CGAL::ORIGIN, p1);
            n_neighbours++;

            hc++;
        } while(hc != hc_end);

        neighbour_center = neighbour_center.transform(
            {CGAL::Scaling(), 1, static_cast<FT>(n_neighbours)});


        if(total_angle < min_angle) {
            vit->point() = neighbour_center;
        }
    }
}


} // namespace meshrrill

#endif // MESHRRILL_POLYHEDRON_FLATTEN_SHARP_POINTS_HPP
