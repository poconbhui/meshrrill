#ifndef MESHRRILL_POLYHEDRON_IMAGETOPOLYHEDRON_HPP
#define MESHRRILL_POLYHEDRON_IMAGETOPOLYHEDRON_HPP


#include <meshrrill/geometry/Kernel.hpp>

#include <CGAL/Modifier_base.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>


namespace meshrrill {

//
// Build a polyhedron from an Image.
// An image class looks like
//  class Image {
//  public:
//      std::size_t nx, ny, nz;
//      double vx, vy, vz;
//      uint32 value(x, y, z);
//  };
//  where
//      nx,ny,nz are the extents of the image in x,y,z;
//      vx,vy,vz are the x,y,z edge lengths of a voxel;
//      value(x,y,z) return the value of the (x,y,z)th voxel.
//
//  The routine calling value(x,y,z) calls with z in the outermost
//  loop, with pairs of (z,z+1) requested together and z incremented
//  in order, allowing for a caching mechanism when reading layer
//  segmented images.
//
template<typename HDS, typename Image>
class ImageToPolyhedron : public CGAL::Modifier_base<HDS> {
public:
    using Kernel = typename HDS::Traits;

    using Point = typename Kernel::Point_3;
    using FT    = typename Kernel::FT;

    struct Exception : public std::runtime_error {
        explicit Exception(const std::string& what_arg) :
            std::runtime_error(what_arg) {}
    };


    std::shared_ptr<Image> image;


    explicit ImageToPolyhedron(const std::shared_ptr<Image> image) :
        image(image) {}


    // A point comparison function for std::map.
    // Two points are considered the same if they're within a squared
    // distance d2.
    // This is used to avoid adding the same vertex multiple times, and
    // retrieving a vertex already inserted into the polyhedron.
    bool point_compare(const FT d2, const Point& a, const Point& b) const {

        if(CGAL::compare_squared_distance(a, b, d2) == CGAL::SMALLER) {
            return false;
        }

        return a < b;
    }


    void operator()(HDS& hds) override {
        using PointCompare  = std::function<bool(const Point, const Point)>;
        using PointIndexMap = std::map<Point, std::size_t, PointCompare>;

        const std::size_t nx = image->nx;
        const std::size_t ny = image->ny;
        const std::size_t nz = image->nz;

        const FT vx = image->vx;
        const FT vy = image->vy;
        const FT vz = image->vz;

        // Points in map stored as 1.0x1.0x1.0 voxels.
        // If points are within a distance of 0.5, they should be the
        // same point.
        const FT d2 = CGAL::square(FT(0.5));

        PointIndexMap point_index_map(std::bind(
            &ImageToPolyhedron::point_compare, this, d2, std::placeholders::_1,
            std::placeholders::_2));

        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);
        B.begin_surface(0, 0, 0);

        for(std::size_t z = 0; z <= nz; z++) {
            for(std::size_t y = 0; y <= ny; y++) {
                for(std::size_t x = 0; x <= nx; x++) {
                    const bool verbose = false;

                    if(verbose) {
                        std::cout << std::endl << std::endl;
                    }
                    if(verbose) {
                        std::cout << "Layer: (" << x << ", " << y << ", " << z
                                  << ")" << std::endl;
                    }

                    const bool pix_set = x != nx && y != ny && z != nz
                                         && image->value(x, y, z) > 0;

                    const bool top_set = x != nx && y != ny && z != 0
                                         && image->value(x, y, z - 1) > 0;
                    const bool left_set = x != 0 && y != ny && z != nz
                                          && image->value(x - 1, y, z) > 0;
                    const bool front_set = x != nx && y != 0 && z != nz
                                           && image->value(x, y - 1, z) > 0;

                    const auto fx = static_cast<FT>(x);
                    const auto fy = static_cast<FT>(y);
                    const auto fz = static_cast<FT>(z);


                    // List of vertices for top, front and back of a cube,
                    // consistently oriented for triangles as 1,2,3 and 3,4,1.
                    using Points = std::array<Point, 4>;
                    const Points top_points{
                        {{fx + FT(0.0), fy + FT(0.0), fz + FT(0.0)},
                         {fx + FT(0.0), fy + FT(1.0), fz + FT(0.0)},
                         {fx + FT(1.0), fy + FT(1.0), fz + FT(0.0)},
                         {fx + FT(1.0), fy + FT(0.0), fz + FT(0.0)}}};

                    const Points left_points{
                        {{fx + FT(0.0), fy + FT(0.0), fz + FT(0.0)},
                         {fx + FT(0.0), fy + FT(0.0), fz + FT(1.0)},
                         {fx + FT(0.0), fy + FT(1.0), fz + FT(1.0)},
                         {fx + FT(0.0), fy + FT(1.0), fz + FT(0.0)}}};

                    const Points front_points{
                        {{fx + FT(0.0), fy + FT(0.0), fz + FT(0.0)},
                         {fx + FT(1.0), fy + FT(0.0), fz + FT(0.0)},
                         {fx + FT(1.0), fy + FT(0.0), fz + FT(1.0)},
                         {fx + FT(0.0), fy + FT(0.0), fz + FT(1.0)}}};


                    // Have point lists and whether or not to add the points
                    // enumerable so they can all be dealt with in the one loop.
                    const std::array<std::pair<bool, Points>, 3> sides{
                        {{pix_set != top_set, top_points},
                         {pix_set != front_set, front_points},
                         {pix_set != left_set, left_points}}};

                    for(const auto& side : sides) {
                        if(!side.first) {
                            continue;
                        }

                        const Points& points = side.second;

                        auto get_vertex_id = [&](const Point& point) {
                            using PIMit = typename PointIndexMap::iterator;
                            using InsertReturn = std::pair<PIMit, bool>;

                            // Try adding the current point to our map of
                            // points. If it's close to a previously
                            // inserted point, use the previously inserted point
                            // instead.
                            const InsertReturn ir = point_index_map.insert(
                                typename PointIndexMap::value_type(
                                    point, point_index_map.size()));

                            // If point wasn't already in the map
                            if(ir.second) {
                                // Add the vertex to the polyhedron.
                                // Scale the point to the appropriate voxel
                                // sizes.
                                B.add_vertex(Point(
                                    ir.first->first.x() * vx,
                                    ir.first->first.y() * vy,
                                    ir.first->first.z() * vz));
                            }

                            // Set facet_vertex_ids[i] to the new or found
                            // index.
                            return ir.first->second;
                        };
                        const std::array<std::size_t, 4> facet_vertex_ids{
                            {get_vertex_id(points[0]), get_vertex_id(points[1]),
                             get_vertex_id(points[2]),
                             get_vertex_id(points[3])}};


                        // Orientation of the facets change if we're on the
                        // inside, looking out and adding facets, or on the
                        // outside looking in and adding facets

                        auto add_facet = [&](std::array<std::size_t, 3> facet) {
                            // If we're outside looking in, flip the
                            // orientation.
                            if(!pix_set) {
                                std::swap(facet[0], facet[2]);
                            }
                            // There are some situations where more than 2
                            // facets will want to share an edge. In this case,
                            // just drop the facet, and deal with it later
                            // during hole filling.
                            if(B.test_facet(
                                   std::begin(facet), std::end(facet))) {
                                B.add_facet(std::begin(facet), std::end(facet));
                            }
                        };
                        add_facet({{facet_vertex_ids[0], facet_vertex_ids[1],
                                    facet_vertex_ids[2]}});
                        add_facet({{facet_vertex_ids[2], facet_vertex_ids[3],
                                    facet_vertex_ids[0]}});
                    }
                }
            }
        }

        // Some vertices have been added, but the facets weren't added
        // because of the multiple shared edges problem. We just remove
        // these.
        B.remove_unconnected_vertices();
        B.end_surface();


        // Close all open borders
        hds.normalize_border();

        CGAL::HalfedgeDS_decorator<HDS> D(hds);
        // We use a while loop to restart the halfedges for loop when a
        // change has been made to the halfedge data structure while closing
        // an open border.
        while(true) {
            bool done = true;

            for(typename HDS::Halfedge_iterator hit = hds.halfedges_begin();
                hit != hds.halfedges_end(); hit++) {
                if(hit->is_border()) {
                    // Fill the hole and triangulate it.
                    typename HDS::Halfedge_handle h = D.fill_hole(hit);
                    D.create_center_vertex(h);

                    // A change has been made. Mark the while loop to run
                    // again and break the current for loop.
                    done = false;
                    break;
                }
            }

            // If we got out the other end of the for loop without
            // encountering a border halfedge, we're done.
            if(done) {
                break;
            }
        }
    }
};


} // namespace meshrrill

#endif // MESHRRILL_POLYHEDRON_IMAGETOPOLYHEDRON_HPP
