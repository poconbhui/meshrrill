#ifndef MESHRRILL_ELLIPSOID_ELLIPSOIDMESHER_HPP
#define MESHRRILL_ELLIPSOID_ELLIPSOIDMESHER_HPP

#include "meshrrill/geometry/ImplicitMesher.hpp"


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// EllipsoidMesher                                                           //
///////////////////////////////////////////////////////////////////////////////
//
// A mesher class for an ellipsoid.
//
// An Ellipsoid volume is defined using the formula:
//     x^2/A^2 + y^2/B^2 + z^2/C^2 <= 1
//
// A, B and C, then, correspond to the ellipsoid semi-axes, analogous
// to a sphere radius. When A = B = C, they are a sphere radius.
//
class EllipsoidMesher : public ImplicitMesher {
public:
    // Initialize, setting A, B, C as found in the ellipsoid function
    // and cell_size, the target size of the mesh cells.
    EllipsoidMesher(double a, double b, double c, double cell_size);


    ~EllipsoidMesher() override = default;
};

} // namespace meshrrill

#endif // MESHRRILL_ELLIPSOID_ELLIPSOIDMESHER_HPP
