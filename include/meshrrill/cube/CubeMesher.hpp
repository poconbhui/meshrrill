#ifndef MESHRRILL_CUBE_CUBEMESHER_HPP
#define MESHRRILL_CUBE_CUBEMESHER_HPP

#include "meshrrill/polyhedron/PolyhedronMesher.hpp"

namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// CubeMesher                                                                //
///////////////////////////////////////////////////////////////////////////////
//
// A mesher class which generates a Cube.
//
class CubeMesher : public PolyhedronMesher {
public:
    CubeMesher(double len_x, double len_y, double len_z, double cell_size);
};


} // namespace meshrrill


#endif // MESHRRILL_CUBE_CUBEMESHER_HPP
