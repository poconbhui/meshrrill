#ifndef MESHRRILL_OCTAHEDRON_OCTAHEDRONMESHER_HPP
#define MESHRRILL_OCTAHEDRON_OCTAHEDRONMESHER_HPP

#include "meshrrill/polyhedron/PolyhedronMesher.hpp"


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// OctahedronMesher                                                          //
///////////////////////////////////////////////////////////////////////////////
//
// A mesher class which generates a truncated octahedron.
//
class OctahedronMesher : public PolyhedronMesher {
public:
    OctahedronMesher(
        double len_x, double len_y, double len_z, double truncation,
        double cell_size);
};


} // namespace meshrrill


#endif // MESHRRILL_OCTAHEDRON_OCTAHEDRONMESHER_HPP
