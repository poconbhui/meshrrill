#ifndef MESHRRILL_MULTIDOMAIN_MULTIDOMAINMESHER_HPP
#define MESHRRILL_MULTIDOMAIN_MULTIDOMAINMESHER_HPP

#include <meshrrill/geometry/GeometryMesher.hpp>
#include <meshrrill/geometry/MeshDomain.hpp>

#include <meshrrill/geometry/MeshDomainWithFeatures3Wrap.hpp>

#include <CGAL/Labeled_mesh_domain_3.h>
#include <CGAL/Mesh_domain_with_polyline_features_3.h>

#include <CGAL/Mesh_3/mesh_standard_criteria.h>

#include <boost/bimap.hpp>

namespace meshrrill {


template<typename MeshDomain>
class LMD3FunctionWrap {
public:
    using Point       = typename MeshDomain::Point_3;
    using return_type = typename MeshDomain::Subdomain_index;

    class Function {
    public:
        virtual return_type operator()(const Point&, bool = true) const = 0;
    };

    const Function* function;

    LMD3FunctionWrap(const Function* function) : function(function) {
        std::cerr << "Labeled_mesh_domain_3_FunctionInterface::Constructor"
                  << std::endl;
    }

    virtual return_type operator()(const Point& point, bool mid = true) const {
        return function->operator()(point, mid);
    }
};


class SubDomain {
public:
    using MeshDomain     = GeometryMesher::MeshDomain;
    using MeshDomain_ptr = std::shared_ptr<MeshDomain>;

    using MeshCriteria     = GeometryMesher::MeshCriteria;
    using MeshCriteria_ptr = std::shared_ptr<MeshCriteria>;

    enum Operation { Add, Subtract, Intersect };

    MeshDomain_ptr mesh_domain;
    MeshCriteria_ptr mesh_criteria;
    Operation operation;

    SubDomain(
        MeshDomain_ptr mesh_domain, MeshCriteria_ptr mesh_criteria,
        Operation operation) :
        mesh_domain(mesh_domain),
        mesh_criteria(mesh_criteria), operation(operation) {}
};


class MultiDomainDomain
    : public MeshDomainWithFeatures3Wrap<
          CGAL::Mesh_domain_with_polyline_features_3<
              CGAL::Labeled_mesh_domain_3<
                  LMD3FunctionWrap<GeometryMesher::MeshDomain>,
                  GeometryMesher::Kernel>>>,
      public LMD3FunctionWrap<GeometryMesher::MeshDomain>::Function {
public:
    using Base = MeshDomainWithFeatures3Wrap<
        CGAL::Mesh_domain_with_polyline_features_3<CGAL::Labeled_mesh_domain_3<
            LMD3FunctionWrap<GeometryMesher::MeshDomain>,
            GeometryMesher::Kernel>>>;

    using FT       = Base::FT;
    using Point_3  = Base::Point_3;
    using Point    = Point_3;
    using Sphere_3 = Base::Sphere_3;

    // bidirectional map of
    // Corner_index -> SubDomain index, SubDomain Corner_index
    using CornerIndexMap =
        boost::bimap<Corner_index, std::pair<std::size_t, Corner_index>>;

    // bidirectional map of
    // Curve_segment_index -> SubDomain index, SubDomain Curve_segment_index
    using CurveSegmentIndexMap = boost::bimap<
        Curve_segment_index, std::pair<std::size_t, Curve_segment_index>>;


    std::shared_ptr<std::vector<SubDomain>> subdomains;
    CornerIndexMap corner_index_map;
    CurveSegmentIndexMap curve_segment_index_map;

    MultiDomainDomain(
        std::shared_ptr<std::vector<SubDomain>> subdomains,
        const Sphere_3& bounding_sphere, const FT& domain_resolution) :
        Base(this, bounding_sphere, domain_resolution),
        subdomains(subdomains), curve_segment_index_map() {}


    void add_subdomain(SubDomain subdomain, bool include_features = false) {
        subdomains->push_back(subdomain);

        if(include_features) {

            std::size_t subdomain_id = subdomains->size() - 1;


            //
            // Add corners
            //

            GetCornersImplContainer tmp_corners;

            subdomains->operator[](subdomain_id)
                .mesh_domain->get_corners_impl(std::back_inserter(tmp_corners));


            for(std::size_t i = 0; i < tmp_corners.size(); i++) {
                CornerIndexMap::right_const_iterator sd_to_md_it =
                    corner_index_map.right.find(CornerIndexMap::right_key_type(
                        subdomain_id, tmp_corners[i].first));

                if(sd_to_md_it == corner_index_map.right.end()) {
                    using InsertReturn =
                        std::pair<CornerIndexMap::right_iterator, bool>;

                    InsertReturn insert_return = corner_index_map.right.insert(
                        CornerIndexMap::right_value_type(
                            CornerIndexMap::right_key_type(
                                subdomain_id, tmp_corners[i].first),
                            CornerIndexMap::right_data_type(
                                corner_index_map.size() + 1)));
                }
            }


            //
            // Add curve segments
            //

            GetCurveSegmentsImplContainer tmp_curve_segments;

            subdomains->operator[](subdomain_id)
                .mesh_domain->get_curve_segments_impl(
                    std::back_inserter(tmp_curve_segments));

            // Convert SubDomain index to global index and register
            // mapped value. Use tmp_curve_segments position as mapped value.
            for(std::size_t i = 0; i < tmp_curve_segments.size(); i++) {
                GetCurveSegmentsImpl& curve_segment = tmp_curve_segments[i];

                // Add <subdomain id, curve_index> value
                CurveSegmentIndexMap::right_const_iterator sd_to_md_it =
                    curve_segment_index_map.right.find(
                        CurveSegmentIndexMap::right_key_type(
                            subdomain_id, std::get<0>(curve_segment)));

                if(sd_to_md_it == curve_segment_index_map.right.end()) {
                    using InsertReturn =
                        std::pair<CurveSegmentIndexMap::right_iterator, bool>;

                    InsertReturn insert_return =
                        curve_segment_index_map.right.insert(
                            CurveSegmentIndexMap::right_value_type(
                                CurveSegmentIndexMap::right_key_type(
                                    subdomain_id, std::get<0>(curve_segment)),
                                CurveSegmentIndexMap::right_data_type(
                                    curve_segment_index_map.size() + 1)));
                }
            }
        }
    }


    // Multidomain level function

    virtual Subdomain_index
    operator()(const Point& point, bool mid = true) const {

        Subdomain_index last_index = Subdomain_index();

        for(std::size_t i = 0; i < subdomains->size(); i++) {
            const SubDomain& s = (*subdomains)[i];

            bool in_domain = false;
            if(s.mesh_domain->is_in_domain_object()(point)) {
                in_domain = true;
            }

            switch(s.operation) {
            case SubDomain::Operation::Add:
                if(in_domain && last_index == Subdomain_index()) {
                    last_index = i + 1;
                }
                break;

            case SubDomain::Operation::Subtract:
                if(in_domain) {
                    last_index = Subdomain_index();
                }
                break;

            case SubDomain::Operation::Intersect:
                if(!in_domain) {
                    last_index = Subdomain_index();
                }
                break;
            }
        }

        return last_index;
    }


    //
    // Multidomain features
    //

    // Retrieval/generation of the input features and their incidences
    // Generate map between global corner/segment indices and subdomain + local
    // corner/segment indices

    virtual GetCornersImplIterator
    get_corners_impl(GetCornersImplIterator impl_it) const {
        GetCornersImplContainer tmp_corners;

        for(std::size_t sd_idx = 0; sd_idx < subdomains->size(); sd_idx++) {
            std::size_t tmp_start = tmp_corners.size();

            subdomains->operator[](sd_idx).mesh_domain->get_corners_impl(
                std::back_inserter(tmp_corners));


            for(std::size_t i = tmp_start; i < tmp_corners.size(); i++) {
                CornerIndexMap::right_const_iterator sd_to_md_it =
                    corner_index_map.right.find(CornerIndexMap::right_key_type(
                        sd_idx, tmp_corners[i].first));

                // If corner has been added to the map, add it to
                // the impl_it iterator.
                if(sd_to_md_it != corner_index_map.right.end()) {
                    tmp_corners[i].first = sd_to_md_it->second;

                    *impl_it = tmp_corners[i];
                    impl_it++;
                }
            }
        }

        return impl_it;
    }

    virtual GetCurveSegmentsImplIterator
    get_curve_segments_impl(GetCurveSegmentsImplIterator impl_it) const {
        GetCurveSegmentsImplContainer tmp_curve_segments;

        for(std::size_t sd_idx = 0; sd_idx < subdomains->size(); sd_idx++) {
            std::size_t tmp_start = tmp_curve_segments.size();

            subdomains->operator[](sd_idx).mesh_domain->get_curve_segments_impl(
                std::back_inserter(tmp_curve_segments));

            // Convert SubDomain index to global index and register
            // mapped value. Use tmp_curve_segments position as mapped value.
            for(std::size_t i = tmp_start; i < tmp_curve_segments.size(); i++) {
                GetCurveSegmentsImpl& curve_segment = tmp_curve_segments[i];

                // Find mapped <subdomain id, curve_index> value
                CurveSegmentIndexMap::right_const_iterator sd_to_md_it =
                    curve_segment_index_map.right.find(
                        CurveSegmentIndexMap::right_key_type(
                            sd_idx, std::get<0>(curve_segment)));

                // If curve segment has been added to the map, add it to
                // the iterator.
                if(sd_to_md_it != curve_segment_index_map.right.end()) {

                    // Set curve_index to map index
                    std::get<0>(curve_segment) = sd_to_md_it->second;

                    // Set corner indices to global values
                    std::get<1>(curve_segment).second =
                        corner_index_map.right.at(
                            CornerIndexMap::right_key_type(
                                sd_idx,
                                boost::get<Subdomain_index>(
                                    std::get<1>(curve_segment).second)));

                    std::get<2>(curve_segment).second =
                        corner_index_map.right.at(
                            CornerIndexMap::right_key_type(
                                sd_idx,
                                boost::get<Subdomain_index>(
                                    std::get<2>(curve_segment).second)));

                    *impl_it = curve_segment;
                    impl_it++;
                }
            }
        }

        return impl_it;
    }


    // Operations / Queries
    // Map back from global corner/segment indices to subdomain + local indices
    // and pass op/query + local indices to the appropriate subdomain.


    // Operations

    virtual Point_3 construct_point_on_curve_segment(
        const Point_3& p, const Curve_segment_index& ci, FT d) const {
        CurveSegmentIndexMap::left_const_iterator csi =
            curve_segment_index_map.left.find(ci);

        return subdomains->operator[](csi->second.first)
            .mesh_domain->construct_point_on_curve_segment(
                p, Curve_segment_index(csi->second.second), d);
    }


    // Queries

    virtual FT geodesic_distance(
        const Point_3& p, const Point_3& q,
        const Curve_segment_index& ci) const {
        CurveSegmentIndexMap::left_const_iterator csi =
            curve_segment_index_map.left.find(ci);

        return subdomains->operator[](csi->second.first)
            .mesh_domain->geodesic_distance(p, q, csi->second.second);
    }

    virtual CGAL::Sign distance_sign_along_cycle(
        const Point_3& p, const Point_3& q, const Point_3& r,
        const Curve_segment_index& ci) const {
        CurveSegmentIndexMap::left_const_iterator csi =
            curve_segment_index_map.left.find(ci);

        return subdomains->operator[](csi->second.first)
            .mesh_domain->distance_sign_along_cycle(
                p, q, r, csi->second.second);
    }

    virtual bool
    is_cycle(const Point_3& p, const Curve_segment_index& ci) const {
        CurveSegmentIndexMap::left_const_iterator csi =
            curve_segment_index_map.left.find(ci);

        return subdomains->operator[](csi->second.first)
            .mesh_domain->is_cycle(p, csi->second.second);
    }
};


class MultiDomainCriteria {
public:
    using SubDomains_ptr        = std::shared_ptr<std::vector<SubDomain>>;
    using MultiDomainDomain_ptr = std::shared_ptr<MultiDomainDomain>;

    using MeshCriteria     = SubDomain::MeshCriteria;
    using MeshCriteria_ptr = SubDomain::MeshCriteria_ptr;

    using FT = SubDomain::MeshDomain::FT;


    class DelegatingMeshEdgeCriteria : public MeshCriteria::Edge_criteria {
    public:
        MultiDomainDomain_ptr multi_domain_domain;

        DelegatingMeshEdgeCriteria(MultiDomainDomain_ptr multi_domain_domain) :
            multi_domain_domain(multi_domain_domain) {}

        virtual FT sizing_field(
            const Point_3& p, const Dim& dim, const Index& index) const {
            std::size_t s_index                        = -1;
            MultiDomainDomain::Subdomain_index e_index = -1;

            // Index refers to corner_index if dim == 0 and curve_segment_index
            // if dim == 1.
            if(dim == 0) {

                MultiDomainDomain::CornerIndexMap::left_const_iterator cit =
                    multi_domain_domain->corner_index_map.left.find(
                        boost::get<MultiDomainDomain::Subdomain_index>(index));
                s_index = cit->second.first;
                e_index = cit->second.second;

            } else if(dim == 1) {

                MultiDomainDomain::CurveSegmentIndexMap::left_const_iterator
                    csit =
                        multi_domain_domain->curve_segment_index_map.left.find(
                            boost::get<MultiDomainDomain::Subdomain_index>(
                                index));
                s_index = csit->second.first;
                e_index = csit->second.second;
            }

            return multi_domain_domain->subdomains->operator[](s_index)
                .mesh_criteria->edge_criteria_object()
                .sizing_field(p, dim, Index(e_index));
        }
    };

    class DelegatingMeshFacetCriteria : public MeshCriteria::Facet_criteria {
    public:
        SubDomains_ptr subdomains;

        DelegatingMeshFacetCriteria(SubDomains_ptr subdomains) :
            subdomains(subdomains) {}

        virtual Is_facet_bad operator()(Facet f) const {

            // Find subdomain of facet
            int index_1 = -1;
            int index_2 = -1;
            try {
                std::pair<int, int> p = boost::get<std::pair<int, int>>(
                    f.first->get_facet_surface_center_index(f.second));
                index_1 = p.first;
                index_2 = p.second;
            } catch(...) {
                return Is_facet_bad();
            }
            if(index_1 > index_2) std::swap(index_1, index_2);

            std::size_t index = -1;
            if(index_1 != 0) {
                index = index_1;
            } else {
                index = index_2;
            }

            // CGAL domains 1 indexed, std::vector 0 indexed
            index -= 1;

            if(index <= 0) {
                return Is_facet_bad();
            }

            return subdomains->operator[](index)
                .mesh_criteria->facet_criteria_object()(f);
        }
    };

    class DelegatingMeshCellCriteria : public MeshCriteria::Cell_criteria {
    public:
        SubDomains_ptr subdomains;

        DelegatingMeshCellCriteria(SubDomains_ptr subdomains) :
            subdomains(subdomains) {}

        virtual Is_cell_bad operator()(Cell_handle c) const {
            Triangulation::Cell::Subdomain_index index = c->subdomain_index();

            // CGAL domains 1 indexed, std::vector 0 indexed
            index -= 1;

            return subdomains->operator[](index)
                .mesh_criteria->cell_criteria_object()(c);
        }
    };


    MultiDomainDomain_ptr multi_domain_domain;
    std::shared_ptr<GeometryMesher::MeshCriteria> mesh_criteria;

    MultiDomainCriteria(MultiDomainDomain_ptr multi_domain_domain) :
        multi_domain_domain(multi_domain_domain) {
        std::shared_ptr<MeshCriteria::Edge_criteria> edge_criteria =
            std::make_shared<DelegatingMeshEdgeCriteria>(multi_domain_domain);

        std::shared_ptr<MeshCriteria::Facet_criteria> facet_criteria =
            std::make_shared<DelegatingMeshFacetCriteria>(
                multi_domain_domain->subdomains);

        std::shared_ptr<MeshCriteria::Cell_criteria> cell_criteria =
            std::make_shared<DelegatingMeshCellCriteria>(
                multi_domain_domain->subdomains);

        mesh_criteria = std::make_shared<GeometryMesher::MeshCriteria>(
            edge_criteria, facet_criteria, cell_criteria);
    }
};


class MultiDomainMesher : public GeometryMesher {
public:
    using MeshDomain = MultiDomainDomain;

    std::shared_ptr<std::vector<SubDomain>> subdomains;
    std::shared_ptr<MultiDomainDomain> multi_domain_domain;
    std::shared_ptr<MultiDomainCriteria> multi_domain_criteria;


    MultiDomainMesher(
        const Kernel::Sphere_3& bounding_sphere,
        const Kernel::FT& domain_resolution) :
        subdomains(std::make_shared<std::vector<SubDomain>>()),
        multi_domain_domain(std::make_shared<MultiDomainDomain>(
            subdomains, bounding_sphere, domain_resolution)),
        multi_domain_criteria(
            std::make_shared<MultiDomainCriteria>(multi_domain_domain)) {}

    void add(const GeometryMesher& mesher, bool include_features = false) {
        multi_domain_domain->add_subdomain(
            SubDomain(
                mesher.mesh_domain(), mesher.mesh_criteria(),
                SubDomain::Operation::Add),
            include_features);
    }


    std::shared_ptr<GeometryMesher::MeshDomain> mesh_domain() const {
        return multi_domain_domain;
    }

    std::shared_ptr<GeometryMesher::MeshCriteria> mesh_criteria() const {
        return multi_domain_criteria->mesh_criteria;
    }
};


} // namespace meshrrill


#endif // MESHRRILL_MULTIDOMAIN_MULTIDOMAINMESHER_HPP
