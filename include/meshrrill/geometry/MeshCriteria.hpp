#ifndef MESHRRILL_GEOMETRY_MESHCRITERIA_HPP
#define MESHRRILL_GEOMETRY_MESHCRITERIA_HPP


#include "meshrrill/geometry/Mesh.hpp"

#include <CGAL/Mesh_3/mesh_standard_cell_criteria.h>
#include <CGAL/Mesh_criteria_3.h>

#include <utility>


namespace meshrrill {


template<typename Triangulation_ = defaults::Mesh::Triangulation>
class MeshEdgeCriteria {
public:
    using Triangulation = Triangulation_;

    using FT      = typename Triangulation::Geom_traits::FT;
    using Point_3 = typename Triangulation::Geom_traits::Point_3;
    using Dim     = int;
    using Index   = typename Triangulation::Vertex::Index;

    virtual FT sizing_field(
        const Point_3& p, const Dim& dim, const Index& index) const = 0;
};

template<typename Triangulation_ = defaults::Mesh::Triangulation>
class MeshFacetCriteria {
public:
    using Triangulation          = Triangulation_;
    using CGAL_MeshFacetCriteria = CGAL::Mesh_facet_criteria_3<Triangulation>;

    using Facet         = typename Triangulation::Facet;
    using Cell_handle   = typename Triangulation::Cell_handle;
    using Facet_quality = typename CGAL_MeshFacetCriteria::Facet_quality;
    using Is_facet_bad  = typename CGAL_MeshFacetCriteria::Facet_badness;

    virtual Is_facet_bad operator()(Facet f) const = 0;

    virtual Is_facet_bad operator()(Cell_handle c, int i) const {
        Facet f;
        f.first  = c;
        f.second = i;

        return operator()(f);
    }
};

template<typename Triangulation_ = defaults::Mesh::Triangulation>
class MeshCellCriteria {
public:
    using Triangulation         = Triangulation_;
    using CGAL_MeshCellCriteria = CGAL::Mesh_cell_criteria_3<Triangulation>;

    using Cell_handle  = typename Triangulation::Cell_handle;
    using Cell_quality = typename CGAL_MeshCellCriteria::Cell_quality;
    using Is_cell_bad  = typename CGAL_MeshCellCriteria::Cell_badness;

    virtual Is_cell_bad operator()(Cell_handle c) const = 0;
};


///////////////////////////////////////////////////////////////////////////
// MeshCriteria                                                          //
///////////////////////////////////////////////////////////////////////////
//
// A class wrapping CGAL::Mesh_criteria_3.
//
template<
    typename Triangulation_  = defaults::Mesh::Triangulation,
    typename Edge_criteria_  = MeshEdgeCriteria<Triangulation_>,
    typename Facet_criteria_ = MeshFacetCriteria<Triangulation_>,
    typename Cell_criteria_  = MeshCellCriteria<Triangulation_>>
class MeshCriteria {
public:
    using Triangulation  = Triangulation_;
    using Edge_criteria  = Edge_criteria_;
    using Facet_criteria = Facet_criteria_;
    using Cell_criteria  = Cell_criteria_;


    std::shared_ptr<Edge_criteria> edge_criteria;
    std::shared_ptr<Facet_criteria> facet_criteria;
    std::shared_ptr<Cell_criteria> cell_criteria;


    MeshCriteria(
        std::shared_ptr<Edge_criteria> edge_criteria,
        std::shared_ptr<Facet_criteria> facet_criteria,
        std::shared_ptr<Cell_criteria> cell_criteria) :
        edge_criteria(std::move(edge_criteria)),
        facet_criteria(std::move(facet_criteria)),
        cell_criteria(std::move(cell_criteria)) {}

    virtual const Edge_criteria& edge_criteria_object() const {
        return *edge_criteria;
    }

    virtual const Facet_criteria& facet_criteria_object() const {
        return *facet_criteria;
    }

    virtual const Cell_criteria& cell_criteria_object() const {
        return *cell_criteria;
    }
};

template<typename Triangulation, typename CGALBase_>
class MeshEdgeCriteria_Wrap : public MeshEdgeCriteria<Triangulation>,
                              public CGALBase_ {
public:
    using MeshrrillBase = MeshEdgeCriteria<Triangulation>;
    using CGALBase      = CGALBase_;

    using FT      = typename MeshrrillBase::FT;
    using Point_3 = typename MeshrrillBase::Point_3;
    using Dim     = typename MeshrrillBase::Dim;
    using Index   = typename MeshrrillBase::Index;

    template<typename... T>
    explicit MeshEdgeCriteria_Wrap(const T&... t) :
        MeshrrillBase(), CGALBase(t...) {}

    FT sizing_field(
        const Point_3& p, const Dim& dim, const Index& index) const override {
        return CGALBase::sizing_field(p, dim, index);
    }
};

template<typename Triangulation, typename CGALBase_>
class MeshFacetCriteria_Wrap : public MeshFacetCriteria<Triangulation>,
                               public CGALBase_ {
public:
    using MeshrrillBase = MeshFacetCriteria<Triangulation>;
    using CGALBase      = CGALBase_;

    using Facet         = typename MeshrrillBase::Facet;
    using Cell_handle   = typename MeshrrillBase::Cell_handle;
    using Facet_quality = typename MeshrrillBase::Facet_quality;
    using Is_facet_bad  = typename MeshrrillBase::Is_facet_bad;

    template<typename... T>
    explicit MeshFacetCriteria_Wrap(const T&... t) :
        MeshrrillBase(), CGALBase(t...) {}

    Is_facet_bad operator()(Facet f) const override {
        return CGALBase::operator()(f);
    }
};

template<typename Triangulation, typename CGALBase_>
class MeshCellCriteria_Wrap : public MeshCellCriteria<Triangulation>,
                              public CGALBase_ {
public:
    using MeshrrillBase = MeshCellCriteria<Triangulation>;
    using CGALBase      = CGALBase_;

    using Cell_handle  = typename MeshrrillBase::Cell_handle;
    using Cell_quality = typename MeshrrillBase::Cell_quality;
    using Is_cell_bad  = typename MeshrrillBase::Is_cell_bad;

    template<typename... T>
    explicit MeshCellCriteria_Wrap(const T&... t) :
        MeshrrillBase(), CGALBase(t...) {}

    Is_cell_bad operator()(Cell_handle c) const override {
        return CGALBase::operator()(c);
    }
};


// Compile the default template
extern template class MeshCriteria<>;
extern template class MeshCellCriteria<>;

namespace defaults {
using MeshCellCriteria = MeshCellCriteria<>;
using MeshCriteria     = MeshCriteria<>;
} // namespace defaults


} // namespace meshrrill


#endif // MESHRRILL_GEOMETRY_MESHCRITERIA_HPP
