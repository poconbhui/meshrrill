#ifndef MESHRRILL_GEOMETRY_IMPLICITMESHER_HPP
#define MESHRRILL_GEOMETRY_IMPLICITMESHER_HPP

#include "meshrrill/geometry/GeometryMesher.hpp"
#include "meshrrill/geometry/MeshDomainWithFeatures3Wrap.hpp"

#include <CGAL/Implicit_mesh_domain_3.h>
#include <CGAL/Mesh_domain_with_polyline_features_3.h>

#include <functional>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// ImplicitMesher                                                            //
///////////////////////////////////////////////////////////////////////////////
//
// A mesher class for an implicit function.
//
class ImplicitMesher : public GeometryMesher {
public:
    using Function = std::function<FT(const Point&)>;
    using Sphere   = Kernel::Sphere_3;

    using MeshDomain =
        MeshDomainWithFeatures3Wrap<CGAL::Mesh_domain_with_polyline_features_3<
            CGAL::Implicit_mesh_domain_3<Function, Kernel>>>;

    ImplicitMesher(
        const Function& function, Sphere bounding_sphere, FT cell_size,
        FT error_bound);

    ~ImplicitMesher() override = default;

    std::shared_ptr<GeometryMesher::MeshDomain> mesh_domain() const override;

    std::shared_ptr<MeshCriteria> mesh_criteria() const override;

protected:
    std::shared_ptr<GeometryMesher::MeshDomain> implicit_domain;
    FT cell_size;
};


} // namespace meshrrill


#endif // MESHRRILL_GEOMETRY_IMPLICITMESHER_HPP
