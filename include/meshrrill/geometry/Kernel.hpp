#ifndef MESHRRILL_GEOMETRY_KERNEL_HPP
#define MESHRRILL_GEOMETRY_KERNEL_HPP


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>


namespace meshrrill {


// Define a the default Kernel for MEshRRILL as EPICK.
namespace defaults {
struct Kernel : public CGAL::Exact_predicates_inexact_constructions_kernel {};
} // namespace defaults


} // namespace meshrrill


#endif // MESHRRILL_GEOMETRY_KERNEL_HPP
