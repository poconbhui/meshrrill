#ifndef MESHRRILL_GEOMETRY_MESHITERATOR_HPP
#define MESHRRILL_GEOMETRY_MESHITERATOR_HPP

#include <algorithm>
#include <array>
#include <functional>
#include <map>

#include <CGAL/Kernel/global_functions.h>
#include <CGAL/transforming_iterator.h>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// MeshIterator                                                              //
///////////////////////////////////////////////////////////////////////////////
//
// A class that wraps point and cell iterators for a CGAL::C3t3 type in
// a nice interface.
//
// Usually to iterate over a CGAL mesh, you have to iterate over points from
// the triangulation member and cells from the c3t3 separately. Further,
// to enumerate cell points, one needs to generate a vertex handle to
// index map. This is a little cumbersome, so we define a simple class
// that wraps all this up with a nice point iterator which returns the
// points and a connectivity iterator which returns lists of point indices
// which make up the cells.
//
// This is super useful for writing mesh files.
//
template<typename Mesh>
class MeshIterator {
public:
    using Tr = typename Mesh::Triangulation;

    using FT = typename Tr::Geom_traits::FT;

    using CellIterator = typename Mesh::Cell_iterator;

    using Vertex_handle          = typename Tr::Vertex_handle;
    using FiniteVerticesIterator = typename Tr::Finite_vertices_iterator;

    using Point         = typename Tr::Point;
    using PointIterator = typename Tr::Point_iterator;


    using Connectivity         = std::array<std::size_t, 4>;
    using Cell                 = typename Tr::Cell;
    using Cell_to_Connectivity = std::function<Connectivity(const Cell&)>;
    using ConnectivityIterator =
        CGAL::transforming_iterator<Cell_to_Connectivity, CellIterator>;

    using PointIndex = std::size_t;


    const Mesh& mesh;
    const Tr& tr;
    std::map<Vertex_handle, PointIndex> vertex_to_index;

    explicit MeshIterator(const Mesh& mesh) :
        mesh(mesh), tr(mesh.triangulation()) {
        // Populate vertex_to_index map which enumerates the vertices
        // in tr.
        PointIndex current_vertex_index = 0;
        for(FiniteVerticesIterator fvit = tr.finite_vertices_begin();
            fvit != tr.finite_vertices_end(); fvit++, current_vertex_index++) {
            vertex_to_index[fvit] = current_vertex_index;
        }
    }


    //
    // Mesh point iterators
    //
    PointIterator points_begin() const {
        return tr.points_begin();
    }

    PointIterator points_end() const {
        return tr.points_end();
    }


    //
    // Mesh connectivity iterators
    //

    ConnectivityIterator connectivities_begin() const {
        return CGAL::make_transforming_iterator(
            mesh.cells_in_complex_begin(),
            std::bind(
                &MeshIterator::cell_to_connectivity, this,
                std::placeholders::_1));
    }

    ConnectivityIterator connectivities_end() const {
        return CGAL::make_transforming_iterator(
            mesh.cells_in_complex_end(), Cell_to_Connectivity());
    }


    // Convert a cell from a set of vertex_handles to a set of point indices
    // referencing their positions from points_begin to points_end.
    Connectivity cell_to_connectivity(const Cell& cit) const {

        using CellVertex   = std::pair<PointIndex, Vertex_handle>;
        using CellVertices = std::array<CellVertex, 4>;

        // Get vertex index and handle pairs
        CellVertices cell_vertices{
            {CellVertex(vertex_to_index.at(cit.vertex(0)), cit.vertex(0)),
             CellVertex(vertex_to_index.at(cit.vertex(1)), cit.vertex(1)),
             CellVertex(vertex_to_index.at(cit.vertex(2)), cit.vertex(2)),
             CellVertex(vertex_to_index.at(cit.vertex(3)), cit.vertex(3))}};

        // Sort so vertex indices are in order.
        // (std::pair sorts by first element first)
        // MERRILL does some index sorting on facets, but I'm
        // not sure if it will do a full sort ... ?
        std::sort(std::begin(cell_vertices), std::end(cell_vertices));

        // Ensure volume is positive.
        // I'm not sure if MERRILL cares what direction the normals
        // actually are as long as they're consistent.
        // Having the same volume ensures the facet normals
        // point in consistent directions, eg always into or out of the
        // cell. It's in, I think, for positive volume ... ?
        FT volume = CGAL::volume(
            cell_vertices[0].second->point().point(),
            cell_vertices[1].second->point().point(),
            cell_vertices[2].second->point().point(),
            cell_vertices[3].second->point().point());

        // Using volume > 0 is probably fine here, but a user could be using
        // some funny lazy evaluation type, so let's be funny too.
        if(CGAL::is_negative(volume)) {
            std::swap(cell_vertices[2], cell_vertices[3]);
        }

        // Recheck volume and complain if it hasn't changed!
        volume = CGAL::volume(
            cell_vertices[0].second->point().point(),
            cell_vertices[1].second->point().point(),
            cell_vertices[2].second->point().point(),
            cell_vertices[3].second->point().point());

        if(CGAL::is_negative(volume)) {
            throw std::runtime_error("Volume Still Negative!");
        }

        return Connectivity{{cell_vertices[0].first, cell_vertices[1].first,
                             cell_vertices[2].first, cell_vertices[3].first}};
    }
};


} // namespace meshrrill


#endif // MESHRRILL_GEOMETRY_MESHITERATOR_HPP
