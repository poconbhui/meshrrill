#ifndef MESHRRILL_GEOMETRY_MESH_HPP
#define MESHRRILL_GEOMETRY_MESH_HPP


#include "meshrrill/geometry/MeshDomain.hpp"
#include "meshrrill/geometry/MeshIterator.hpp"

#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_triangulation_3.h>


namespace meshrrill {


// Helper class to extract C3t3 from MeshDomain.
template<typename MeshDomain>
struct Mesh_helper {
    using Triangulation = typename CGAL::Mesh_triangulation_3<MeshDomain>::type;
    using C3t3 =
        typename CGAL::Mesh_complex_3_in_triangulation_3<Triangulation>;
};


///////////////////////////////////////////////////////////////////////////
// Mesh                                                                  //
///////////////////////////////////////////////////////////////////////////
//
// Mesh class which inherits from C3t3 found in MeshDomain.
// Default MeshDomain is the default MEshRRILL MeshDomain.
//
// This will be used in the GeometryMesher class, returned by the make_mesh
// method. It's basically a CGAL::C3t3, but with a few bells and whistles.
// Notably, the get_mesh_iterator method returns a MeshIterator which
// plays rather nicely with mesh file IO.
//
template<typename MeshDomain = defaults::MeshDomain>
class Mesh : public Mesh_helper<MeshDomain>::C3t3 {
public:
    using Self = Mesh<MeshDomain>;

    using C3t3 = typename Mesh_helper<MeshDomain>::C3t3;

    using MeshIterator = typename meshrrill::MeshIterator<Self>;

    MeshIterator get_mesh_iterator() const {
        return MeshIterator(*this);
    }
};


// Compile default template
extern template class Mesh<>;

namespace defaults {
using Mesh = Mesh<>;
} // namespace defaults


} // namespace meshrrill


#endif // MESHRRILL_GEOMETRY_MESH_HPP
