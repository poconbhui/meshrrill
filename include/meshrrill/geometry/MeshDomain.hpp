#ifndef MESHRRILL_GEOMETRY_MESHDOMAIN_HPP
#define MESHRRILL_GEOMETRY_MESHDOMAIN_HPP

#include "meshrrill/geometry/Kernel.hpp"

#include <CGAL/enum.h>
#include <CGAL/tags.h>
#include <CGAL/tuple.h>
#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <memory>
#include <utility>
#include <vector>

namespace meshrrill {


///////////////////////////////////////////////////////////////////////////
// MeshDomain                                                            //
///////////////////////////////////////////////////////////////////////////
//
// A class that provides a CGAL::MeshDomainWithFeatures_3 base class
// which is suitable for polymorphism. That is, all the methods required
// by the MeshDomainWithFeatures_3 concept are provided here as virtual
// members.
//
// This is useful for defining a base mesher class which other meshers
// can inherit from and redefine meshing functions etc. This is useful
// for holding a list of meshers, which we really want for options parsing
// in the CLI interface.
//
// This probably runs slower than the generic programming style CGAL
// interface, but I find it's much easier to extend.
//
template<typename Kernel = meshrrill::defaults::Kernel>
class MeshDomain {
public:
    // Geometric object types
    using FT        = typename Kernel::FT;
    using Point_3   = typename Kernel::Point_3;
    using Segment_3 = typename Kernel::Segment_3;
    using Ray_3     = typename Kernel::Ray_3;
    using Line_3    = typename Kernel::Line_3;
    using Vector_3  = typename Kernel::Vector_3;
    using Sphere_3  = typename Kernel::Sphere_3;


    // Kernel_traits compatibility
    using R = Kernel;


    //-------------------------------------------------------
    // Index Types
    //-------------------------------------------------------

    // Type of indexes for cells of the input complex
    using Subdomain_index = int;
    using Subdomain       = boost::optional<Subdomain_index>;

    // Type of indexes for surface patch of the input complex
    using Surface_patch_index = std::pair<Subdomain_index, Subdomain_index>;
    using Surface_patch       = boost::optional<Surface_patch_index>;

    // Type of indexes to characterize the lowest dimensional face
    // of the input complex on which a vertex lie
    using Index        = boost::variant<Subdomain_index, Surface_patch_index>;
    using Intersection = CGAL::cpp11::tuple<Point_3, Index, int>;

    // Indices for features
    using Curve_segment_index = int;
    using Corner_index        = int;

    using Has_features = CGAL::Tag_true;


    // Add a virtual destructor
    virtual ~MeshDomain() = default;


    //
    // MeshDomain_3
    //

    // Operations

    // Redefining Construct_initial_points to provide concrete, overloadable
    // implementations for the generic functions. Probably not the most
    // efficient, but oh well... Impl approach is since return values
    // with this class aren't reference or pointer or anything.
    using ConstructInitialPointsImplContainer =
        std::vector<std::pair<Point_3, Index>>;

    using ConstructInitialPointsImplIterator =
        std::back_insert_iterator<ConstructInitialPointsImplContainer>;

    struct Construct_initial_points_impl {
        using InitialPointsContainer = ConstructInitialPointsImplContainer;
        using InitialPointsIterator  = ConstructInitialPointsImplIterator;

        virtual InitialPointsIterator
        operator()(InitialPointsIterator initial_points_iterator) = 0;

        virtual InitialPointsIterator
        operator()(InitialPointsIterator initial_points_iterator, int n) = 0;
    };

    struct Construct_initial_points {
        using InitialPointsContainer = ConstructInitialPointsImplContainer;
        using InitialPointsIterator  = ConstructInitialPointsImplIterator;

        explicit Construct_initial_points(
            std::shared_ptr<Construct_initial_points_impl> impl) :
            impl(std::move(impl)) {}

        template<typename OutputIterator>
        OutputIterator operator()(OutputIterator pts) {
            InitialPointsContainer initial_points_container;

            impl->operator()(std::back_inserter(initial_points_container));

            return std::copy(
                initial_points_container.begin(),
                initial_points_container.end(), pts);
        }

        template<typename OutputIterator>
        OutputIterator operator()(OutputIterator pts, int n) {
            InitialPointsContainer initial_points_container;

            impl->operator()(std::back_inserter(initial_points_container), n);

            return std::copy(
                initial_points_container.begin(),
                initial_points_container.end(), pts);
        }


        std::shared_ptr<Construct_initial_points_impl> impl;
    };


    struct Is_in_domain_impl {
        virtual Subdomain operator()(Point_3 p) = 0;
    };

    struct Is_in_domain {
        explicit Is_in_domain(std::shared_ptr<Is_in_domain_impl> impl) :
            impl(std::move(impl)) {}

        Subdomain operator()(Point_3 p) {
            return impl->operator()(p);
        }

        std::shared_ptr<Is_in_domain_impl> impl;
    };


    struct Do_intersect_surface_impl {
        virtual Surface_patch operator()(Segment_3 s) = 0;

        virtual Surface_patch operator()(Ray_3 r) = 0;

        virtual Surface_patch operator()(Line_3 l) = 0;
    };

    struct Do_intersect_surface {
        explicit Do_intersect_surface(
            std::shared_ptr<Do_intersect_surface_impl> impl) :
            impl(std::move(impl)) {}

        virtual Surface_patch operator()(Segment_3 s) {
            return impl->operator()(s);
        }

        virtual Surface_patch operator()(Ray_3 r) {
            return impl->operator()(r);
        }

        virtual Surface_patch operator()(Line_3 l) {
            return impl->operator()(l);
        }

        std::shared_ptr<Do_intersect_surface_impl> impl;
    };


    struct Construct_intersection_impl {
        virtual Intersection operator()(Segment_3 s) = 0;

        virtual Intersection operator()(Ray_3 r) = 0;

        virtual Intersection operator()(Line_3 l) = 0;
    };

    struct Construct_intersection {
        explicit Construct_intersection(
            std::shared_ptr<Construct_intersection_impl> impl) :
            impl(std::move(impl)) {}

        virtual Intersection operator()(Segment_3 s) {
            return impl->operator()(s);
        }

        virtual Intersection operator()(Ray_3 r) {
            return impl->operator()(r);
        }

        virtual Intersection operator()(Line_3 l) {
            return impl->operator()(l);
        }

        std::shared_ptr<Construct_intersection_impl> impl;
    };

    virtual Construct_initial_points
    construct_initial_points_object() const = 0;

    virtual Is_in_domain is_in_domain_object() const = 0;

    virtual Do_intersect_surface do_intersect_surface_object() const = 0;

    virtual Construct_intersection construct_intersection_object() const = 0;


    // Index conversion

    virtual Index index_from_surface_patch_index(
        const Surface_patch_index& surface_patch_index) const = 0;

    virtual Index index_from_subdomain_index(
        const Subdomain_index& subdomain_index) const = 0;

    virtual Surface_patch_index
    surface_patch_index(const Index& index) const = 0;

    virtual Subdomain_index subdomain_index(const Index& index) const = 0;


    //
    // MeshDomainWithFeatures_3
    //

    // Operations

    virtual Point_3 construct_point_on_curve_segment(
        const Point_3& p, const Curve_segment_index& ci, FT d) const = 0;


    // Queries

    virtual FT geodesic_distance(
        const Point_3& p, const Point_3& q,
        const Curve_segment_index& ci) const = 0;

    virtual CGAL::Sign distance_sign_along_cycle(
        const Point_3& p, const Point_3& q, const Point_3& r,
        const Curve_segment_index& ci) const = 0;

    virtual bool
    is_cycle(const Point_3& p, const Curve_segment_index& ci) const = 0;


    // Retrieval of the input features and their incidences

    using GetCornersImpl = std::pair<Corner_index, Point_3>;

    using GetCornersImplContainer = std::vector<GetCornersImpl>;

    using GetCornersImplIterator =
        std::back_insert_iterator<GetCornersImplContainer>;

    template<typename OutputIterator>
    OutputIterator get_corners(OutputIterator corners) const {
        GetCornersImplContainer impl_it;
        get_corners_impl(std::back_inserter(impl_it));
        return std::copy(impl_it.begin(), impl_it.end(), corners);
    }

    virtual GetCornersImplIterator
    get_corners_impl(GetCornersImplIterator impl_it) const = 0;


    using GetCurveSegmentsImpl = CGAL::cpp11::tuple<
        Curve_segment_index, std::pair<Point_3, Index>,
        std::pair<Point_3, Index>>;

    using GetCurveSegmentsImplContainer = std::vector<GetCurveSegmentsImpl>;

    using GetCurveSegmentsImplIterator =
        std::back_insert_iterator<GetCurveSegmentsImplContainer>;

    template<typename OutputIterator>
    OutputIterator get_curve_segments(OutputIterator curves) const {
        GetCurveSegmentsImplContainer impl_it;
        get_curve_segments_impl(std::back_inserter(impl_it));
        return std::copy(impl_it.begin(), impl_it.end(), curves);
    }

    virtual GetCurveSegmentsImplIterator
    get_curve_segments_impl(GetCurveSegmentsImplIterator impl_it) const = 0;


    // virtual bool are_incident_surface_patch_curve_segment(
    //     Surface_patch_index spi, Curve_segment_index csi
    // ) = 0;

    // virtual bool are_incident_surface_patch_corner(
    //     Surface_patch_index spi, Corner_index ci
    // ) = 0;


    // Indices converters

    virtual Index index_from_curve_segment_index(
        const Curve_segment_index& curve_segment_index) const = 0;

    virtual Curve_segment_index
    curve_segment_index(const Index& index) const = 0;

    virtual Index
    index_from_corner_index(const Corner_index& corner_index) const = 0;

    virtual Corner_index corner_index(const Index& index) const = 0;
};


// Compile default template
extern template class MeshDomain<>;

namespace defaults {

using MeshDomain = MeshDomain<>;

} // namespace defaults


} // namespace meshrrill

#endif // MESHRRILL_GEOMETRY_MESHDOMAIN_HPP
