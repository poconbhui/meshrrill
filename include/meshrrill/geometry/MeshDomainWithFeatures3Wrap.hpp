#ifndef MESHRRILL_GEOMETRY_MESHDOMAINWITHFEATURES3WRAP_HPP
#define MESHRRILL_GEOMETRY_MESHDOMAINWITHFEATURES3WRAP_HPP

#include "meshrrill/geometry/MeshDomain.hpp"

#include <CGAL/enum.h>
#include <CGAL/tuple.h>
#include <boost/optional.hpp>

namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// MeshDomainWithFeatures3Wrap                                               //
///////////////////////////////////////////////////////////////////////////////
//
// A class which wraps all the methods in a CGALMeshDomainWithFeatures_3
// concept class with the MeshDomain interface so it can be used nicely with
// polymorphic MEshRRILL classes like GeometryMesher which expect an
// inheriting class to return instances of MeshDomain.
//
template<
    typename CGALMeshDomainWithFeatures_3,
    typename Kernel = typename CGALMeshDomainWithFeatures_3::R>
class MeshDomainWithFeatures3Wrap : public meshrrill::MeshDomain<Kernel>,
                                    public CGALMeshDomainWithFeatures_3 {
private:
    using MeshrrillBase = meshrrill::MeshDomain<Kernel>;
    using CGALBase      = CGALMeshDomainWithFeatures_3;

public:
    // Geometric object types
    using FT        = typename MeshrrillBase::FT;
    using Point_3   = typename MeshrrillBase::Point_3;
    using Segment_3 = typename MeshrrillBase::Segment_3;
    using Ray_3     = typename MeshrrillBase::Ray_3;
    using Line_3    = typename MeshrrillBase::Line_3;
    using Vector_3  = typename MeshrrillBase::Vector_3;
    using Sphere_3  = typename MeshrrillBase::Sphere_3;


    // Kernel_traits compatibility
    using R = typename MeshrrillBase::R;


    // Index Types
    using Subdomain_index = typename MeshrrillBase::Subdomain_index;
    using Subdomain       = typename MeshrrillBase::Subdomain;

    using Surface_patch_index = typename MeshrrillBase::Surface_patch_index;
    using Surface_patch       = typename MeshrrillBase::Surface_patch;

    using Index        = typename MeshrrillBase::Index;
    using Intersection = typename MeshrrillBase::Intersection;

    using Curve_segment_index = typename MeshrrillBase::Curve_segment_index;
    using Corner_index        = typename MeshrrillBase::Corner_index;

    using Has_features = typename MeshrrillBase::Has_features;


    // Construct whatever CGALBase
    template<typename... T>
    explicit MeshDomainWithFeatures3Wrap(const T&... t) :
        MeshrrillBase(), CGALBase(t...) {}

    ~MeshDomainWithFeatures3Wrap() override = default;


    //
    // MeshDomain_3
    //

    // Operations

    struct Construct_initial_points_impl
        : public MeshrrillBase::Construct_initial_points_impl,
          public CGALBase::Construct_initial_points {

        explicit Construct_initial_points_impl(
            const typename CGALBase::Construct_initial_points& b) :
            MeshrrillBase::Construct_initial_points_impl(),
            CGALBase::Construct_initial_points(b) {}

        using InitialPointsIterator = typename MeshrrillBase::
            Construct_initial_points_impl::InitialPointsIterator;

        InitialPointsIterator
        operator()(InitialPointsIterator initial_points_iterator) override {
            return CGALBase::Construct_initial_points::operator()(
                initial_points_iterator);
        };

        InitialPointsIterator operator()(
            InitialPointsIterator initial_points_iterator, int n) override {
            return CGALBase::Construct_initial_points::operator()(
                initial_points_iterator, n);
        };
    };

    struct Is_in_domain_impl : public MeshrrillBase::Is_in_domain_impl,
                               public CGALBase::Is_in_domain {
        explicit Is_in_domain_impl(const typename CGALBase::Is_in_domain& b) :
            MeshrrillBase::Is_in_domain_impl(), CGALBase::Is_in_domain(b) {}

        Subdomain operator()(Point_3 p) override {
            return CGALBase::Is_in_domain::operator()(p);
        }
    };

    struct Do_intersect_surface_impl
        : public MeshrrillBase::Do_intersect_surface_impl,
          public CGALBase::Do_intersect_surface {
        explicit Do_intersect_surface_impl(
            const typename CGALBase::Do_intersect_surface& b) :
            MeshrrillBase::Do_intersect_surface_impl(),
            CGALBase::Do_intersect_surface(b) {}

        Surface_patch operator()(Segment_3 s) override {
            return CGALBase::Do_intersect_surface::operator()(s);
        }

        Surface_patch operator()(Ray_3 r) override {
            return CGALBase::Do_intersect_surface::operator()(r);
        }


        Surface_patch operator()(Line_3 l) override {
            return CGALBase::Do_intersect_surface::operator()(l);
        }
    };

    struct Construct_intersection_impl
        : public MeshrrillBase::Construct_intersection_impl,
          public CGALBase::Construct_intersection {
        explicit Construct_intersection_impl(
            const typename CGALBase::Construct_intersection& b) :
            MeshrrillBase::Construct_intersection_impl(),
            CGALBase::Construct_intersection(b) {}

        Intersection operator()(Segment_3 s) override {
            return CGALBase::Construct_intersection::operator()(s);
        }

        Intersection operator()(Ray_3 r) override {
            return CGALBase::Construct_intersection::operator()(r);
        }

        Intersection operator()(Line_3 l) override {
            return CGALBase::Construct_intersection::operator()(l);
        }
    };


    typename MeshrrillBase::Construct_initial_points
    construct_initial_points_object() const override {
        return typename MeshrrillBase::Construct_initial_points(
            std::make_shared<Construct_initial_points_impl>(
                CGALBase::construct_initial_points_object()));
    }

    typename MeshrrillBase::Is_in_domain is_in_domain_object() const override {
        return typename MeshrrillBase::Is_in_domain(
            std::make_shared<Is_in_domain_impl>(
                CGALBase::is_in_domain_object()));
    }

    typename MeshrrillBase::Do_intersect_surface
    do_intersect_surface_object() const override {
        return typename MeshrrillBase::Do_intersect_surface(
            std::make_shared<Do_intersect_surface_impl>(
                CGALBase::do_intersect_surface_object()));
    }

    typename MeshrrillBase::Construct_intersection
    construct_intersection_object() const override {
        return typename MeshrrillBase::Construct_intersection(
            std::make_shared<Construct_intersection_impl>(
                CGALBase::construct_intersection_object()));
    }


    // Index conversion

    Index index_from_surface_patch_index(
        const Surface_patch_index& surface_patch_index) const override {
        return CGALBase::index_from_surface_patch_index(surface_patch_index);
    }

    Index index_from_subdomain_index(
        const Subdomain_index& subdomain_index) const override {
        return CGALBase::index_from_subdomain_index(subdomain_index);
    }

    Surface_patch_index surface_patch_index(const Index& index) const override {
        return CGALBase::surface_patch_index(index);
    }

    Subdomain_index subdomain_index(const Index& index) const override {
        return CGALBase::subdomain_index(index);
    }


    //
    // MeshDomainWithFeatures_3
    //

    // Operations

    Point_3 construct_point_on_curve_segment(
        const Point_3& p, const Curve_segment_index& ci, FT d) const override {
        return CGALBase::construct_point_on_curve_segment(p, ci, d);
    }


    // Queries

    FT geodesic_distance(
        const Point_3& p, const Point_3& q,
        const Curve_segment_index& ci) const override {
        return CGALBase::geodesic_distance(p, q, ci);
    }

    CGAL::Sign distance_sign_along_cycle(
        const Point_3& p, const Point_3& q, const Point_3& r,
        const Curve_segment_index& ci) const override {
        return CGALBase::distance_sign_along_cycle(p, q, r, ci);
    }

    bool
    is_cycle(const Point_3& p, const Curve_segment_index& ci) const override {
        return CGALBase::is_cycle(p, ci);
    }


    // Retrieval of the input features and their incidences

    using GetCornersImplIterator =
        typename MeshrrillBase::GetCornersImplIterator;

    GetCornersImplIterator
    get_corners_impl(GetCornersImplIterator impl_it) const override {
        return CGALBase::get_corners(impl_it);
    }


    using GetCurveSegmentsImplIterator =
        typename MeshrrillBase::GetCurveSegmentsImplIterator;

    GetCurveSegmentsImplIterator get_curve_segments_impl(
        GetCurveSegmentsImplIterator impl_it) const override {
        return CGALBase::get_curve_segments(impl_it);
    }


    // virtual bool are_incident_surface_patch_curve_segment(
    //     Surface_patch_index spi, Curve_segment_index csi
    // ) {
    //     return CGALBase::are_incident_surface_patch_curve_segment(spi, csi);
    // }

    // virtual bool are_incident_surface_patch_corner(
    //     Surface_patch_index spi, Corner_index ci
    // ) {
    //     return CGALBase::are_incident_surface_patch_corner(spi, ci);
    // }


    // Indices converters

    Index index_from_curve_segment_index(
        const Curve_segment_index& curve_segment_index) const override {
        return CGALBase::index_from_curve_segment_index(curve_segment_index);
    }

    Curve_segment_index curve_segment_index(const Index& index) const override {
        return CGALBase::curve_segment_index(index);
    }

    Index
    index_from_corner_index(const Corner_index& corner_index) const override {
        return CGALBase::index_from_corner_index(corner_index);
    }

    Corner_index corner_index(const Index& index) const override {
        return CGALBase::corner_index(index);
    }
};

} // namespace meshrrill

#endif // MESHRRILL_GEOMETRY_MESHDOMAINWITHFEATURES3WRAP_HPP
