#ifndef MESHRRILL_GEOMETRY_GEOMETRYMESHER_HPP
#define MESHRRILL_GEOMETRY_GEOMETRYMESHER_HPP

#include "meshrrill/geometry/Kernel.hpp"
#include "meshrrill/geometry/Mesh.hpp"
#include "meshrrill/geometry/MeshCriteria.hpp"
#include "meshrrill/geometry/MeshDomain.hpp"

#define DEBUG_TRACE
#ifndef CGAL_MESH_3_VERBOSE
#define CGAL_MESH_3_VERBOSE 1
#endif


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// GeometryMesher                                                            //
///////////////////////////////////////////////////////////////////////////////
//
// A base class for defining all the bits and pieces necessary to generate
// a mesh.
//
// An inheriting class should overload the mesh_domain method to return an
// instance of MeshDomain. The class will probably also define a class
// inheriting MeshDomain and return it from the mesh_domain method.
//
class GeometryMesher {
public:
    //
    // CGAL types
    //

    // Kernel, Floating point type, Point type.
    using Kernel = meshrrill::defaults::Kernel;
    using FT     = Kernel::FT;
    using Point  = Kernel::Point_3;

    // Domain
    using MeshDomain = defaults::MeshDomain;

    // Triangulation and Mesh
    using Mesh          = defaults::Mesh;
    using Triangulation = Mesh::Triangulation;

    // Criteria
    using MeshCriteria = defaults::MeshCriteria;


    // Virtual destructor
    virtual ~GeometryMesher() = default;


    // This is the important bit to define.
    virtual std::shared_ptr<MeshDomain> mesh_domain() const = 0;


    // Return the meshing criteria. ie the minimum quality criteria
    // the mesh should meet.
    virtual std::shared_ptr<MeshCriteria> mesh_criteria() const = 0;


    // Generate a new C3t3/Mesh mesh and return it.
    virtual std::shared_ptr<Mesh> mesh() const;


    // Set the input Mesh to the mesh defined by the mesh_domain and
    // the mesh_criteria.
    virtual std::shared_ptr<Mesh> make_mesh() const;

    virtual void make_mesh(
        Mesh& mesh, const MeshDomain& mesh_domain,
        const MeshCriteria& mesh_criteria) const;


    // Optimize the input mesh using various techniques.
    virtual void optimize_mesh(Mesh& mesh) const;


    // Optimize the input mesh using various techniques with the
    // input mesh_domain as a guide for those optimizers.
    virtual void optimize_mesh(Mesh& mesh, const MeshDomain& mesh_domain) const;
};

} // namespace meshrrill

#endif // MESHRRILL_GEOMETRY_GEOMETRYMESHER_HPP
