#include "OctahedronOptionsParser.hpp"

#include <iostream>

namespace meshrrill {

std::string OctahedronOptionsParser::usage() const {
    return "usage: #program_name# [global_options] #geometry_name# X Y Z [t]";
}

std::string OctahedronOptionsParser::description() const {
    return "Generate a (truncated) octahedron with corner to corner lengths of"
           " X, Y, Z, and a truncation factor t.";
}

boost::program_options::options_description
OctahedronOptionsParser::options() const {
    namespace po = boost::program_options;

    po::options_description octahedron_options("Octahedron Geometry Options");
    octahedron_options.add_options()(
        ",X",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_lengths, "X", std::placeholders::_1)),
        "The length in the X direction.")(
        ",Y",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_lengths, "Y", std::placeholders::_1)),
        "The length in the Y direction.")(
        ",Z",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_lengths, "Z", std::placeholders::_1)),
        "The length in the Z direction.")(
        ",t",
        po::value<double>()
            ->value_name("<factor>")
            ->notifier(std::bind(check_truncation, std::placeholders::_1)),
        "The amount of truncation (range [0, 1)) (Default: 0).");


    // Add the base mesh_options and these cube_options together.
    po::options_description all_options("Allowed Options");
    all_options.add(Base::options()).add(octahedron_options);

    return all_options;
}

int OctahedronOptionsParser::parse(
    const std::vector<std::string>& args,
    boost::program_options::variables_map& vm) const {
    namespace po = boost::program_options;

    po::options_description octahedron_options = options();

    po::positional_options_description octahedron_positional;
    octahedron_positional.add("-X", 1).add("-Y", 1).add("-Z", 1).add("-t", 1);

    try {
        po::store(
            po::command_line_parser(args)
                .options(octahedron_options)
                .positional(octahedron_positional)
                .run(),
            vm);

        po::notify(vm);
    } catch(po::error& e) {
        std::cout << "Error: OctahedronOptionsParser: " << e.what()
                  << std::endl;

        return Base::PARSE_FAILURE;
    }


    // Apply default arguments
    if(vm.count("-t") == 0) {
        vm.insert(std::make_pair("-t", po::variable_value(double(0), true)));
    }

    return Base::PARSE_SUCCESS;
}

void OctahedronOptionsParser::check_lengths(
    const std::string& name, const double& value) {
    namespace po = boost::program_options;

    if(value <= 0) {
        std::stringstream error_ss;
        error_ss << "Axis " << name << ": Length must be > 0.";
        throw po::error(error_ss.str());
    }
}

void OctahedronOptionsParser::check_truncation(const double& value) {
    namespace po = boost::program_options;

    if(value < 0 || value >= 1.0) {
        std::stringstream error_ss;
        error_ss << "Truncation value must be in range [0, 1).";
        throw po::error(error_ss.str());
    }
}

} // namespace meshrrill
