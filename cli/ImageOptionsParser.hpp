#ifndef MESHRRILL_CLI_IMAGEOPTIONSPARSER_HPP
#define MESHRRILL_CLI_IMAGEOPTIONSPARSER_HPP

#include "GeometryOptionsParser.hpp"

#include <boost/program_options.hpp>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// ImageOptionsParser                                                        //
///////////////////////////////////////////////////////////////////////////////
//
// An implementation of GeometryOptionsParser to parse command line arguments
// for an ImageManager.
//
class ImageOptionsParser : public GeometryOptionsParser {
public:
    using Base = GeometryOptionsParser;


    // A usage string for the Image geometry.
    std::string usage() const override;


    // A quick description of the Image geometry.
    std::string description() const override;


    // The program options for the Image geometry.
    boost::program_options::options_description options() const override;


    // Parse the arguments for the Image into the variable_map vm.
    int parse(
        const std::vector<std::string>& args,
        boost::program_options::variables_map& vm) const override;


    //
    // Option validators
    //

    // Check axes are sane.
    static void check_axis(const std::string& name, const double& v);
    static void check_smoothing(const double& v);
    static void check_threshold(const double& v);
};


} // namespace meshrrill


#endif // MESHRRILL_CLI_IMAGEOPTIONSPARSER_HPP
