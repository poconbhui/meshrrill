#ifndef MESHRRILL_CLI_CUBEOPTIONSPARSER_HPP
#define MESHRRILL_CLI_CUBEOPTIONSPARSER_HPP

#include "GeometryOptionsParser.hpp"

namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// CubeOptionsParser                                                         //
///////////////////////////////////////////////////////////////////////////////
//
// A parser class for parsing CLI options for CubeMesher.
//
class CubeOptionsParser : public GeometryOptionsParser {
public:
    using Base = GeometryOptionsParser;

    std::string usage() const override;

    std::string description() const override;

    boost::program_options::options_description options() const override;

    int parse(
        const std::vector<std::string>& args,
        boost::program_options::variables_map& vm) const override;

    static void check_lengths(const std::string& name, const double& value);
};

} // namespace meshrrill

#endif // MESHRRILL_CLI_CUBEOPTIONSPARSER_HPP
