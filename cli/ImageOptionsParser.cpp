#include "ImageOptionsParser.hpp"

#include <iostream>
#include <sstream>

namespace meshrrill {

namespace po = boost::program_options;


std::string ImageOptionsParser::usage() const {
    return "usage: #program_name# [global_options] #geometry_name# image_file"
           " X Y Z [--smooth <value>]";
}


std::string ImageOptionsParser::description() const {
    return "Generate a mesh of a 3D Image with voxel dimensions"
           " (X, Y, Z).";
}


po::options_description ImageOptionsParser::options() const {

    po::options_description image_options("Image Geometry Options");
    image_options.add_options()(
        "image",
        po::value<std::string>()->value_name("<image_file>")->required(),
        "The image file to mesh.")(
        ",X",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_axis, "X", std::placeholders::_1)),
        "The length of a voxel in the x-direction.")(
        ",Y",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_axis, "Y", std::placeholders::_1)),
        "The length of a voxel in the y-direction.")(
        ",Z",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_axis, "Z", std::placeholders::_1)),
        "The length of a voxel in the z-direction.")(
        "smooth",
        po::value<double>()->value_name("<value>")->notifier(check_smoothing),
        "Specify constrained smoothing of the image surface."
        " <value> specifies how many voxels from the starting position a point"
        " is allowed to move during smoothing. (default 1)")(
        "surface", po::value<bool>()->implicit_value(true),
        "Output the reconstructed surface to the mesh file instead of the"
        " volume mesh.");


    // Add the base mesh_options and these cube_options together.
    po::options_description all_options("Allowed Options");
    all_options.add(Base::options()).add(image_options);

    return all_options;
}


int ImageOptionsParser::parse(
    const std::vector<std::string>& args,
    boost::program_options::variables_map& vm) const {

    po::options_description image_options = options();

    po::positional_options_description image_positional;
    image_positional.add("image", 1).add("-X", 1).add("-Y", 1).add("-Z", 1);

    try {
        po::store(
            po::command_line_parser(args)
                .options(image_options)
                .positional(image_positional)
                .run(),
            vm);

        po::notify(vm);
    } catch(po::error& e) {
        std::cout << "Error: ImageOptionsParser: " << e.what() << std::endl;

        return Base::PARSE_FAILURE;
    }

    return Base::PARSE_SUCCESS;
}


void ImageOptionsParser::check_axis(const std::string& name, const double& v) {
    if(v <= 0) {
        std::stringstream error_ss;
        error_ss << "Length of voxel " << name << "-dimension must be > 0.";
        throw po::error(error_ss.str());
    }
}

void ImageOptionsParser::check_smoothing(const double& v) {
    if(v < 0) {
        std::stringstream error_ss;
        error_ss << "Smoothing value must be >= 0.";
        throw po::error(error_ss.str());
    }
}

void ImageOptionsParser::check_threshold(const double& v) {
    if(v < 0) {
        std::stringstream error_ss;
        error_ss << "Threshold value must be >= 0.";
        throw po::error(error_ss.str());
    }
}


} // namespace meshrrill
