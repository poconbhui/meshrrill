#include "MEshRRILLOptionsParser.hpp"

#include <iostream>
#include <utility>


namespace meshrrill {


namespace po = boost::program_options;


MEshRRILLOptionsParser::MEshRRILLOptionsParser() = default;


// Add a parser for a subcommand to the MEshRRILLOptionsParser
void MEshRRILLOptionsParser::add_subcommand_parser(
    const GeometryOptionsParserMap::key_type& geometry_name,
    GeometryOptionsParserMap::mapped_type geometry_options_parser) {
    if(geometry_options_parser_map.count(geometry_name) != 0) {
        std::stringstream error_ss;
        error_ss << "Duplicate subcommand name: " << geometry_name;
        throw std::logic_error(error_ss.str());
    }

    geometry_options_parser_map[geometry_name] =
        std::move(geometry_options_parser);
}


std::string MEshRRILLOptionsParser::program_name() const {
    return "meshrrill";
}


std::string MEshRRILLOptionsParser::usage() const {
    return "usage: #program_name# [global_options] geometry [geometry_options]";
}


std::string MEshRRILLOptionsParser::description() const {
    return "MEshRILL - quickly produce meshes for some simple geometries.";
}


po::options_description MEshRRILLOptionsParser::options() const {

    // Define global options for MEshRILL
    po::options_description options("Global options");
    options.add_options()(
        // We call a subcommand a geometry, since we expect subcommands
        // to be for geometries to mesh
        "geometry,g",
        po::value<std::string>()->value_name("<name>")->required()->notifier(
            std::bind(
                &MEshRRILLOptionsParser::check_subcommand, this,
                std::placeholders::_1)),
        "Generate a mesh for geometry <name>. (Required)")(
        "help,h", "Display this help message.");

    return options;
}


// Parse all the program options, including the options for the
// appropriate SubcommandManager
int MEshRRILLOptionsParser::parse(
    const std::vector<std::string>& raw_args, po::variables_map& vm) const {

    std::vector<std::string> subcommand_options;
    try {
        // Parse global options and initial subcommand name
        subcommand_options = parse_initial_and_global_options(raw_args, vm);
        vm.insert(std::make_pair(
            "subcommand_options",
            po::variable_value(subcommand_options, true)));
    } catch(po::error& e) {
        print_error(e);
        return MEshRRILLOptionsParser::PARSE_FAILURE;
    }

    // Start outputting help if necessary
    if(vm.count("help") != 0) {
        print_help(vm);
        return PARSE_PRINTED_HELP;
    }

    return PARSE_SUCCESS;
}


std::vector<std::string>
MEshRRILLOptionsParser::parse_initial_and_global_options(
    const std::vector<std::string>& args, po::variables_map& vm) const {

    // Get subcommand by position
    po::positional_options_description global_positional;
    global_positional.add("geometry", 1);

    //
    // Add bits for ignoring extra args so they can be parsed by
    // the appropriate subcommand, after we've parsed what the
    // subcommand is.
    //

    // Define initial_options for first parser pass.
    // This includes some stuff for gathering, but ignoring
    // potential options for subcommands
    po::options_description initial_options;
    initial_options.add(options());

    // A silent gatherer for subcommand args so they can be parsed later
    initial_options.add_options()(
        "subcommand_args", po::value<std::vector<std::string>>());

    // Add a positional argument gatherer on top of the global_positional
    // arguments. Note: Later assumptions are made that global_positional
    // contains exactly one positional argument.
    po::positional_options_description initial_positional(global_positional);
    initial_positional.add("subcommand_args", -1);


    // Parsing out here so parsed isn't lost in the try block scope.
    po::parsed_options parsed = po::command_line_parser(args)
                                    .options(initial_options)
                                    .positional(initial_positional)
                                    .allow_unregistered()
                                    .run();


    // Try parsing the initial subcommand and general options
    po::store(parsed, vm);

    std::vector<std::string> subcommand_args;

    if(vm.count("help") == 0) {
        po::notify(vm);

        // Gather all unparsed args for the subcommand parser
        subcommand_args =
            po::collect_unrecognized(parsed.options, po::include_positional);
        // Pop off the subcommand name in the options
        subcommand_args.erase(subcommand_args.begin());
    }
    return subcommand_args;
}


void MEshRRILLOptionsParser::check_subcommand(
    const std::string& subcommand_name) const {

    if(geometry_options_parser_map.count(subcommand_name) == 0) {
        std::stringstream error_ss;
        error_ss << "No geometry/command named "
                 << "\"" << subcommand_name << "\"";
        throw po::error(error_ss.str());
    }
}


// From http://stackoverflow.com/a/3418285
template<typename T>
static void
replace_all(std::string& str, const std::string& from, const T& to) {
    if(from.empty()) {
        return;
    }

    std::stringstream to_ss;
    to_ss << to;

    const std::string& to_str = to_ss.str();

    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to_str);
        // In case 'to' contains 'from', like replacing 'x' with 'yx'
        start_pos += to_str.length();
    }
}


template<typename T>
static void replace_common_options(std::string& str, const T& t) {
    replace_all(str, "#usage#", t.usage());
    replace_all(str, "#description#", t.description());
    replace_all(str, "#options#", t.options());
}


static std::string
parse_program_options_str(std::string str, const MEshRRILLOptionsParser& pop) {
    replace_common_options(str, pop);
    replace_all(str, "#program_name#", pop.program_name());

    return str;
}


static std::string parse_geometry_options_str(
    std::string str, const MEshRRILLOptionsParser& pop,
    const std::string& gop_name, const GeometryOptionsParser& gop) {
    replace_common_options(str, gop);
    str = parse_program_options_str(str, pop);
    replace_all(str, "#geometry_name#", gop_name);

    return str;
}


void MEshRRILLOptionsParser::print_error(po::error& e) const {
    std::cout
        << parse_program_options_str("Error: MEshRRILLOptionsParser: ", *this)
        << e.what() << std::endl;
}


void MEshRRILLOptionsParser::print_help(const po::variables_map& vm) const {

    std::cout << parse_program_options_str(
        "\n"
        "#description#\n"
        "\n"
        "#usage#\n\n"
        "#options#",
        *this);

    // Check if the subcommand has been set properly, and if so,
    // output help for that. If not, list the subcommands.
    bool subcommand_name_ok = false;
    try {
        check_subcommand(vm["geometry"].as<std::string>());
        subcommand_name_ok = true;
    } catch(std::exception& e) {
    }

    if(subcommand_name_ok) {

        std::string subcommand_name = vm["geometry"].as<std::string>();

        GeometryOptionsParserMap::mapped_type geometry_options_parser =
            geometry_options_parser_map.at(subcommand_name);

        std::cout << parse_geometry_options_str(
            "\n\n"
            "Help for geometry: #geometry_name#\n\n"
            "#description#\n\n"
            "#usage#\n\n"
            "#options#",
            *this, subcommand_name, *geometry_options_parser);

    } else {

        std::cout << std::endl << "Supported geometries:" << std::endl;

        for(const auto& gopm_it : geometry_options_parser_map) {
            std::cout << "    " << gopm_it.first << " - "
                      << gopm_it.second->description() << std::endl;
        }

        std::cout
            << parse_program_options_str(
                   "For more information, try #program_name# --help <geometry>",
                   *this)
            << std::endl;
    }
}


} // namespace meshrrill
