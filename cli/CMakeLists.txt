cmake_minimum_required(VERSION 3.4.3)

project(
    MEshRRILL-cli

    VERSION 1.1.1
    LANGUAGES CXX
)


###############################################################################
# Add required libraries. Boost program_options                               #
###############################################################################

# Add Boost ###################################################################

find_package(Boost COMPONENTS program_options REQUIRED)
if(Boost_FOUND)
    message("-- Boost_VERSION: ${Boost_VERSION}")
    message("-- Boost_INCLUDE_DIRS ${Boost_INCLUDE_DIRS}")
    message("-- Boost_LIBRARY_DIRS ${Boost_LIBRARY_DIRS}")
    message("-- Boost_LIBRARIES ${Boost_LIBRARIES}")
    message("-- Boost_PROGRAM_OPTIONS_FOUND ${Boost_PROGRAM_OPTIONS_FOUND}")
    message("-- Boost_PROGRAM_OPTIONS_LIBRARY ${Boost_PROGRAM_OPTIONS_LIBRARY}")
endif()



###############################################################################
# Setup libraries, executables etc.                                           #
###############################################################################


# Add meshrrill executable ####################################################

add_executable(
    meshrrill-cli

    GeometryOptionsParser.cpp

    MEshRRILLOptionsParser.cpp

    OctahedronOptionsParser.cpp
    CubeOptionsParser.cpp

    EllipsoidManager.cpp
    EllipsoidOptionsParser.cpp

    ImageManager.cpp
    ImageOptionsParser.cpp

    meshrrill.main.cpp
)

set_target_properties(
    meshrrill-cli

    PROPERTIES
        CXX_STANDARD 11
        OUTPUT_NAME meshrrill
)

target_link_libraries(meshrrill-cli meshrrill-lib ${Boost_LIBRARIES})



###############################################################################
# Set install targets                                                         #
###############################################################################

install(
    TARGETS meshrrill-cli
    DESTINATION ${MEshRRILL_EXE_INSTALL_DIR}
)
