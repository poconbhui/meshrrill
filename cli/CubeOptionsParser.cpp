#include "CubeOptionsParser.hpp"

#include <iostream>

namespace meshrrill {

std::string CubeOptionsParser::usage() const {
    return "usage: #program_name# [global_options] #geometry_name# X Y Z";
}

std::string CubeOptionsParser::description() const {
    return "Generate an cube with sides of length"
           " X, Y, Z.";
}

boost::program_options::options_description CubeOptionsParser::options() const {
    namespace po = boost::program_options;

    po::options_description cube_options("Cube Geometry Options");
    cube_options.add_options()(
        ",X",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_lengths, "X", std::placeholders::_1)),
        "The length in the X direction.")(
        ",Y",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_lengths, "Y", std::placeholders::_1)),
        "The length in the Y direction.")(
        ",Z",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_lengths, "Z", std::placeholders::_1)),
        "The length in the Z direction.");


    // Add the base mesh_options and these cube_options together.
    po::options_description all_options("Allowed Options");
    all_options.add(Base::options()).add(cube_options);

    return all_options;
}

int CubeOptionsParser::parse(
    const std::vector<std::string>& args,
    boost::program_options::variables_map& vm) const {
    namespace po = boost::program_options;

    po::options_description cube_options = options();

    po::positional_options_description cube_positional;
    cube_positional.add("-X", 1).add("-Y", 1).add("-Z", 1);

    try {
        po::store(
            po::command_line_parser(args)
                .options(cube_options)
                .positional(cube_positional)
                .run(),
            vm);

        po::notify(vm);
    } catch(po::error& e) {
        std::cout << "Error: CubeOptionsParser: " << e.what() << std::endl;

        return Base::PARSE_FAILURE;
    }

    return Base::PARSE_SUCCESS;
}

void CubeOptionsParser::check_lengths(
    const std::string& name, const double& value) {
    namespace po = boost::program_options;

    if(value <= 0) {
        std::stringstream error_ss;
        error_ss << "Axis " << name << ": Length must be > 0.";
        throw po::error(error_ss.str());
    }
}

} // namespace meshrrill
