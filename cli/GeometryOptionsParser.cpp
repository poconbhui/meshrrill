#include "GeometryOptionsParser.hpp"

namespace meshrrill {

namespace po = boost::program_options;

po::options_description GeometryOptionsParser::options() const {

    // Define global options for MEshRILL
    po::options_description options("Meshing Options");
    options.add_options()(
        "cell-size,c",
        po::value<double>()->value_name("<size>")->required()->notifier(
            std::bind(
                &GeometryOptionsParser::check_cell_size, this,
                std::placeholders::_1)),
        "Generate a mesh with cell size <size>. (Required)")(
        "output,o", po::value<std::string>()->value_name("<filename>"),
        "Output mesh in Patran format to <filename>.\n"
        "Default <filename> is mesh.neu")(
        "optimize,O", po::value<bool>(),
        "Optimize the mesh after generation using"
        " a range of perturbation/extrusion techniques.\n"
        "Default on.");


    return options;
}


void GeometryOptionsParser::check_cell_size(const double& v) const {
    if(v <= 0) {
        std::stringstream error_ss;
        error_ss << "Error: Cell size must be > 0.";
        throw po::error(error_ss.str());
    }
}

} // namespace meshrrill
