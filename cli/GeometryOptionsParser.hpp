#ifndef MESHRRILL_CLI_GEOMETRYOPTIONSPARSER_HPP
#define MESHRRILL_CLI_GEOMETRYOPTIONSPARSER_HPP

#include <string>
#include <vector>

#include <boost/program_options.hpp>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// GeometryOptionsParser                                                     //
///////////////////////////////////////////////////////////////////////////////
//
// A base type for parsing options for geometries to mesh.
//
class GeometryOptionsParser {
public:
    enum {
        PARSE_FAILURE = -1,
        PARSE_SUCCESS = 0,
    };


    virtual ~GeometryOptionsParser() = default;


    // A usage message.
    // eg: meshrrill name [options] X Y Z
    virtual std::string usage() const = 0;


    // A short description of the program.
    // eg: Generate an ellipse.
    virtual std::string description() const = 0;


    // The program options for the program.
    virtual boost::program_options::options_description options() const;


    // Accept the vector of strings as program options and add the
    // appropriately parsed options to vm.
    // This assumes that options has already been partially processed by
    // boost::program_options, specifically in the ProgramOptionsManager class.
    virtual int parse(
        const std::vector<std::string>& options,
        boost::program_options::variables_map& vm) const = 0;


    // Check the cell_size options has a sane value.
    void check_cell_size(const double& v) const;
};

} // namespace meshrrill


#endif // MESHRRILL_CLI_GEOMETRYOPTIONSPARSER_HPP
