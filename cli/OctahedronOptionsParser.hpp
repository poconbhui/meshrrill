#ifndef MESHRRILL_CLI_OCTAHEDRONOPTIONSPARSER_HPP
#define MESHRRILL_CLI_OCTAHEDRONOPTIONSPARSER_HPP

#include "GeometryOptionsParser.hpp"


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// OctahedronOptionsParser                                                   //
///////////////////////////////////////////////////////////////////////////////
//
// An options parser for the OctahedronMesher.
//
class OctahedronOptionsParser : public GeometryOptionsParser {
public:
    using Base = GeometryOptionsParser;

    std::string usage() const override;

    std::string description() const override;

    boost::program_options::options_description options() const override;

    int parse(
        const std::vector<std::string>& args,
        boost::program_options::variables_map& vm) const override;

    static void check_lengths(const std::string& name, const double& value);
    static void check_truncation(const double& value);
};

} // namespace meshrrill

#endif //  MESHRRILL_CLI_OCTAHEDRONOPTIONSPARSER_HPP
