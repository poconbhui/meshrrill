#include "EllipsoidManager.hpp"


namespace meshrrill {


std::shared_ptr<EllipsoidManager::Base::OptionsParser>
EllipsoidManager::new_parser() const {
    return std::make_shared<OptionsParser>();
}


std::shared_ptr<EllipsoidManager::Base::Mesher> EllipsoidManager::new_mesher(
    const boost::program_options::variables_map& vm) const {
    double a = vm["-A"].as<double>();
    double b = vm["-B"].as<double>();
    double c = vm["-C"].as<double>();

    double cell_size = vm["cell-size"].as<double>();

    return std::make_shared<Mesher>(a, b, c, cell_size);
}


} // namespace meshrrill
