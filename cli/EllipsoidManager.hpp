#ifndef MESHRRILL_CLI_ELLIPSOIDMANAGER_HPP
#define MESHRRILL_CLI_ELLIPSOIDMANAGER_HPP

#include "EllipsoidOptionsParser.hpp"
#include "GeometryManager.hpp"

#include "meshrrill/ellipsoid/EllipsoidMesher.hpp"


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// EllipsoidManager                                                          //
///////////////////////////////////////////////////////////////////////////////
//
// A manager class for parsing CLI options for EllipsoidManager and running it.
//
class EllipsoidManager : public GeometryManager {
public:
    using Base = GeometryManager;

    using OptionsParser = EllipsoidOptionsParser;
    using Mesher        = EllipsoidMesher;


    // Generate a new EllipsoidOptionsParser.
    std::shared_ptr<Base::OptionsParser> new_parser() const override;


    // Generate a new EllipsoidMesher, using the variable_map vm
    // generated using an EllipsoidOptionsParser.
    std::shared_ptr<Base::Mesher>
    new_mesher(const boost::program_options::variables_map& vm) const override;
};

} // namespace meshrrill

#endif // MESHRRILL_CLI_ELLIPSOIDMANAGER_HPP
