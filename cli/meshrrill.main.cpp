#include "MEshRRILLManager.hpp"

#include <string>
#include <vector>


int main(int argc, char* argv[]) {

    // Turn argc, argv into a vector of args (minus argv[0])
    std::vector<std::string> options;
    for(int i = 1; i < argc; i++) {
        options.push_back(std::string(argv[i])); // NOLINT
    }

    // Run MEshRRILLManager
    int err = meshrrill::MEshRRILLManager().run(options);

    // Exit
    if(err == meshrrill::MEshRRILLManager::RUN_SUCCESS) {
        return EXIT_SUCCESS;
    }

    // Not explicit success, return failure
    return EXIT_FAILURE;
}
