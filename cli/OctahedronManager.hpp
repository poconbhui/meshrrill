#ifndef MESHRRILL_CLI_OCTAHEDRONMANAGER_HPP
#define MESHRRILL_CLI_OCTAHEDRONMANAGER_HPP

#include "GeometryManager.hpp"
#include "OctahedronOptionsParser.hpp"

#include "meshrrill/octahedron/OctahedronMesher.hpp"


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// OctahedronManager                                                         //
///////////////////////////////////////////////////////////////////////////////
//
// A manager class for parsing CLI options for OctahedronMesher and running it.
//
class OctahedronManager : public GeometryManager {
public:
    using Base = GeometryManager;

    using OptionsParser = OctahedronOptionsParser;
    using Mesher        = OctahedronMesher;

    std::shared_ptr<Base::OptionsParser> new_parser() const override {
        return std::make_shared<OptionsParser>();
    }

    std::shared_ptr<Base::Mesher>
    new_mesher(const boost::program_options::variables_map& vm) const override {
        double x = vm["-X"].as<double>();
        double y = vm["-Y"].as<double>();
        double z = vm["-Z"].as<double>();
        double t = vm["-t"].as<double>();

        double cell_size = vm["cell-size"].as<double>();

        return std::make_shared<Mesher>(x, y, z, t, cell_size);
    }
};

} // namespace meshrrill

#endif // MESHRRILL_CLI_OCTAHEDRONMANAGER_HPP
