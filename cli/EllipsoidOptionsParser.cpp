#include "EllipsoidOptionsParser.hpp"

#include <iostream>
#include <sstream>

namespace meshrrill {

namespace po = boost::program_options;


std::string EllipsoidOptionsParser::usage() const {
    return "usage: #program_name# [global_options] #geometry_name# A B C";
}


std::string EllipsoidOptionsParser::description() const {
    return "Generate an ellipsoid of the form"
           " x^2/A^b + y^2/B^2 + z^2/C^2 <= 1.";
}


po::options_description EllipsoidOptionsParser::options() const {

    po::options_description ellipsoid_options("Ellipsoid Geometry Options");
    ellipsoid_options.add_options()(
        ",A",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_axis, "A", std::placeholders::_1)),
        "The length of the x-semi-axes.")(
        ",B",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_axis, "B", std::placeholders::_1)),
        "The length of the y-semi-axes.")(
        ",C",
        po::value<double>()
            ->value_name("<length>")
            ->required()
            ->notifier(std::bind(check_axis, "C", std::placeholders::_1)),
        "The length of the z-semi-axes.");


    // Add the base mesh_options and these cube_options together.
    po::options_description all_options("Allowed Options");
    all_options.add(Base::options()).add(ellipsoid_options);

    return all_options;
}


int EllipsoidOptionsParser::parse(
    const std::vector<std::string>& args,
    boost::program_options::variables_map& vm) const {

    po::options_description ellipsoid_options = options();

    po::positional_options_description ellipsoid_positional;
    ellipsoid_positional.add("-A", 1).add("-B", 1).add("-C", 1);

    try {
        po::store(
            po::command_line_parser(args)
                .options(ellipsoid_options)
                .positional(ellipsoid_positional)
                .run(),
            vm);

        po::notify(vm);
    } catch(po::error& e) {
        std::cout << "Error: EllipsoidOptionsParser: " << e.what() << std::endl;

        return Base::PARSE_FAILURE;
    }

    return Base::PARSE_SUCCESS;
}


void EllipsoidOptionsParser::check_axis(
    const std::string& name, const double& v) {

    if(v <= 0) {
        std::stringstream error_ss;
        error_ss << "Length of axis " << name << " must be > 0.";
        throw po::error(error_ss.str());
    }
}


} // namespace meshrrill
