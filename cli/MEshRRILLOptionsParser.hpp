#ifndef MESHRRILL_CLI_MESHRRILLOPTIONSPARSER_HPP
#define MESHRRILL_CLI_MESHRRILLOPTIONSPARSER_HPP

#include "GeometryOptionsParser.hpp"

#include <boost/program_options.hpp>
#include <memory>
#include <string>
#include <vector>

namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// MEshRRILLOptionsParser                                                    //
///////////////////////////////////////////////////////////////////////////////
//
// A class which parses the options for the MEshRRILL CLI interface.
//
class MEshRRILLOptionsParser {
public:
    using GeometryOptionsParserMap =
        std::map<std::string, std::shared_ptr<GeometryOptionsParser>>;

    enum {
        PARSE_FAILURE      = -1,
        PARSE_SUCCESS      = 0,
        PARSE_PRINTED_HELP = 1,
    };


    MEshRRILLOptionsParser();


    // Add a parser for a subcommand to the MEshRRILLOptionsParser
    void add_subcommand_parser(
        const GeometryOptionsParserMap::key_type& geometry_name,
        GeometryOptionsParserMap::mapped_type geometry_options_parser);


    // The program name
    std::string program_name() const;


    // Return a usage message
    std::string usage() const;


    // Return a short description of the program
    std::string description() const;


    // Return the global options for the program
    boost::program_options::options_description options() const;


    // Parse all the program options, and generate a set of
    // subcommand_options sutable for parsing by a GeometryOptionsParser.
    int parse(
        const std::vector<std::string>& raw_args,
        boost::program_options::variables_map& vm) const;


private:
    // Do the initial parse, find the global options and the name
    // of the geometry to parse. Return the arguments remaining for
    // the GeomertyOptionsParser to parse.
    std::vector<std::string> parse_initial_and_global_options(
        const std::vector<std::string>& args,
        boost::program_options::variables_map& vm) const;


    // Check the requested subcommand_name defines a parser.
    void check_subcommand(const std::string& subcommand_name) const;


    // Print an error message.
    void print_error(boost::program_options::error& e) const;

    // Print a help message.
    void print_help(const boost::program_options::variables_map& vm) const;


    // Storage for the GeometryOptionsParsers.
    GeometryOptionsParserMap geometry_options_parser_map;
};


} // namespace meshrrill

#endif // MESHRRILL_CLI_MESHRRILLOPTIONSPARSER_HPP
