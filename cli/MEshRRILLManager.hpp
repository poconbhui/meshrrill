#ifndef MESHRRILL_CLI_MESHRRILLMANAGER_HPP
#define MESHRRILL_CLI_MESHRRILLMANAGER_HPP

#include "MEshRRILLOptionsParser.hpp"

#include "CubeManager.hpp"
#include "EllipsoidManager.hpp"
#include "GeometryManager.hpp"
#include "ImageManager.hpp"
#include "OctahedronManager.hpp"

#include <memory>
#include <string>
#include <vector>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// MEshRRILLManager                                                          //
///////////////////////////////////////////////////////////////////////////////
//
// A manager class for running the MEshRRILL CLI.
//
class MEshRRILLManager {
public:
    using OptionsParser = MEshRRILLOptionsParser;

    enum { RUN_FAILURE = -1, RUN_SUCCESS = 0 };


    //
    // Map of geometry managers
    //
    using GeometryManagerMap =
        std::map<std::string, std::shared_ptr<GeometryManager>>;

    virtual GeometryManagerMap geometry_manager_map() const {

        GeometryManagerMap geometry_manager_map;

        geometry_manager_map["ellipsoid"] =
            std::make_shared<EllipsoidManager>();
        geometry_manager_map["octahedron"] =
            std::make_shared<OctahedronManager>();
        geometry_manager_map["cube"]  = std::make_shared<CubeManager>();
        geometry_manager_map["image"] = std::make_shared<ImageManager>();

        return geometry_manager_map;
    }


    // Generate a new options parser
    virtual std::shared_ptr<OptionsParser> new_parser() const {
        std::shared_ptr<OptionsParser> options_parser =
            std::make_shared<OptionsParser>();

        // Add all the geometry parsers to the MEshRRILLOptionsParser.
        GeometryManagerMap gmm = geometry_manager_map();

        for(auto& gmm_it : gmm) {
            options_parser->add_subcommand_parser(
                gmm_it.first, gmm_it.second->new_parser());
        }

        return options_parser;
    }


    // Print a try meshrrill --help message.
    virtual void print_try_help(const std::string& geometry) const {

        std::string program_name = new_parser()->program_name();
        std::cout << std::endl
                  << "Try `" << program_name << " --help`"
                  << " or `" << program_name << " --help " << geometry << "`"
                  << " for more information." << std::endl;
    }


    // Run MEshRRILL from a set of input options
    virtual int run(const std::vector<std::string>& argv) const {

        int err;


        //
        // Initial parse for subcommand name and arguments
        //

        boost::program_options::variables_map vm;
        std::shared_ptr<OptionsParser> options_parser = new_parser();
        err = options_parser->parse(argv, vm);

        // Check return value for possible early termination.
        if(err == OptionsParser::PARSE_PRINTED_HELP) {
            return RUN_SUCCESS;
        }
        if(err == OptionsParser::PARSE_FAILURE) {
            print_try_help("<geometry>");
            return RUN_FAILURE;
        }


        //
        // Find and run the required GeometryManager
        //

        // Get the geometry manager
        std::string geometry_name = vm["geometry"].as<std::string>();
        GeometryManagerMap::mapped_type geometry_manager =
            geometry_manager_map()[geometry_name];

        // Get the geometry manager options
        std::vector<std::string> geometry_manager_options =
            vm["subcommand_options"].as<std::vector<std::string>>();

        std::cout << std::endl
                  << "Meshing geometry: " << geometry_name << std::endl
                  << std::endl;


        // Run the geometry manager
        try {
            err = geometry_manager->run(geometry_manager_options);
        } catch(...) {
            print_try_help(geometry_name);
            return RUN_FAILURE;
        }


        // Return appropriate value
        if(err == GeometryManager::RUN_SUCCESS) {
            return RUN_SUCCESS;
        }

        // Not explicit success, return failure.
        print_try_help(geometry_name);
        return RUN_FAILURE;
    }
};

} // namespace meshrrill

#endif // MESHRRILL_CLI_MESHRRILLMANAGER_HPP
