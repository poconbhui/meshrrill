#ifndef MESHRRILL_CLI_GEOMETRYMANAGER_HPP
#define MESHRRILL_CLI_GEOMETRYMANAGER_HPP

#include "GeometryOptionsParser.hpp"
#include "meshrrill/geometry/GeometryMesher.hpp"
#include "meshrrill/io/output_to_patran.hpp"

#include <memory>

namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// GeometryManager                                                           //
///////////////////////////////////////////////////////////////////////////////
//
// A base type which manages parsing CLI options passed to it and runs the
// appropriate program. In particular, this defines meshing a geometry
// and outputting it to a file.
//
class GeometryManager {
public:
    using OptionsParser = GeometryOptionsParser;
    using Mesher        = GeometryMesher;

    enum { RUN_FAILURE = -1, RUN_SUCCESS = 0 };


    // Return a new instance of a GeometryOptionsParser
    virtual std::shared_ptr<OptionsParser> new_parser() const = 0;


    // Return a new instance of a GeometryMesher based on variables
    // map vm parsed with the appropriate parser.
    virtual std::shared_ptr<Mesher>
    new_mesher(const boost::program_options::variables_map& vm) const = 0;


    // Run the Geometry meshing program based on the current arguments.
    virtual int run(const std::vector<std::string>& argv) const {
        // Parse the program options
        boost::program_options::variables_map vm;
        int err = new_parser()->parse(argv, vm);

        if(err == OptionsParser::PARSE_FAILURE) {
            return RUN_FAILURE;
        }


        // Get the program mesher
        std::shared_ptr<Mesher> mesher = new_mesher(vm);

        // If an empty mesher was returned, do nothing.
        if(!mesher) {
            return RUN_SUCCESS;
        }


        // Assume volume mesh. Get the meshed Mesh.
        std::shared_ptr<Mesher::Mesh> mesh = mesher->make_mesh();


        // Optimize if necessary
        // Optimize by default.
        bool do_optimize = true;
        if(vm.count("optimize") > 0) {
            do_optimize = vm["optimize"].as<bool>();
        }

        if(do_optimize) {
            mesher->optimize_mesh(*mesh);
        }


        // Output to disk
        std::string output_filename("mesh.neu");
        if(vm.count("output") > 0) {
            output_filename = vm["output"].as<std::string>();
        }

        std::ofstream pat_file(output_filename.c_str());

        std::cout << "Writing to File..." << std::endl;
        output_mesh_to_patran(pat_file, *mesh);
        std::cout << "- Done." << std::endl;


        return RUN_SUCCESS;
    }
};

} // namespace meshrrill

#endif // MESHRRILL_CLI_GEOMETRYMANAGER_HPP
