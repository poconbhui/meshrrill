#ifndef MESHRRILL_CLI_ELLIPSOIDOPTIONSPARSER_HPP
#define MESHRRILL_CLI_ELLIPSOIDOPTIONSPARSER_HPP

#include "GeometryOptionsParser.hpp"

#include <boost/program_options.hpp>


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// EllipsoidOptionsParser                                                    //
///////////////////////////////////////////////////////////////////////////////
//
// An implementation of GeometryOptionsParser to parse command line arguments
// for an EllipsoidManager.
//
class EllipsoidOptionsParser : public GeometryOptionsParser {
public:
    using Base = GeometryOptionsParser;


    // A usage string for the Ellipsoid geometry.
    std::string usage() const override;


    // A quick description of the Ellipsoid geometry.
    std::string description() const override;


    // The program options for the Ellipsoid geometry.
    boost::program_options::options_description options() const override;


    // Parse the arguments for the Ellipsoid into the variable_map vm.
    int parse(
        const std::vector<std::string>& args,
        boost::program_options::variables_map& vm) const override;


    //
    // Option validators
    //

    // Check axes are sane.
    static void check_axis(const std::string& name, const double& v);
};


} // namespace meshrrill


#endif // MESHRRILL_CLI_ELLIPSOIDOPTIONSPARSER_HPP
