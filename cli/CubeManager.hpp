#ifndef MESHRRILL_CLI_CUBEMANAGER_HPP
#define MESHRRILL_CLI_CUBEMANAGER_HPP

#include "GeometryManager.hpp"

#include "CubeOptionsParser.hpp"
#include "meshrrill/cube/CubeMesher.hpp"

namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// CubeManager                                                               //
///////////////////////////////////////////////////////////////////////////////
//
// A manager class which runs a CLI options parser for CubeMesher and then
// runs the mesher and outputs the mesh to a file.
//
class CubeManager : public GeometryManager {
public:
    using Base = GeometryManager;

    using OptionsParser = CubeOptionsParser;
    using Mesher        = CubeMesher;

    std::shared_ptr<Base::OptionsParser> new_parser() const override {
        return std::make_shared<OptionsParser>();
    }

    std::shared_ptr<Base::Mesher>
    new_mesher(const boost::program_options::variables_map& vm) const override {
        double x = vm["-X"].as<double>();
        double y = vm["-Y"].as<double>();
        double z = vm["-Z"].as<double>();

        double cell_size = vm["cell-size"].as<double>();

        return std::make_shared<Mesher>(x, y, z, cell_size);
    }
};

} // namespace meshrrill

#endif // MESHRRILL_CLI_CUBEMANAGER_HPP
