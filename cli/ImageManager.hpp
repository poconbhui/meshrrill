#ifndef MESHRRILL_CLI_IMAGEMANAGER_HPP
#define MESHRRILL_CLI_IMAGEMANAGER_HPP

#include "GeometryManager.hpp"
#include "ImageOptionsParser.hpp"
#include "meshrrill/polyhedron/PolyhedronMesher.hpp"


namespace meshrrill {


///////////////////////////////////////////////////////////////////////////////
// ImageManager                                                              //
///////////////////////////////////////////////////////////////////////////////
//
// A manager class for parsing CLI options for ImageManager and running it.
//
class ImageManager : public GeometryManager {
public:
    using Base = GeometryManager;

    using OptionsParser = ImageOptionsParser;
    using Mesher        = PolyhedronMesher;


    // Generate a new ImageOptionsParser.
    std::shared_ptr<Base::OptionsParser> new_parser() const override;


    // Generate a new ImageMesher, using the variable_map vm
    // generated using an ImageOptionsParser.
    std::shared_ptr<Base::Mesher>
    new_mesher(const boost::program_options::variables_map& vm) const override;
};

} // namespace meshrrill

#endif // MESHRRILL_CLI_IMAGEMANAGER_HPP
