#include "ImageManager.hpp"

#include "meshrrill/io/CachedTIFFReader.hpp"
#include "meshrrill/io/output_polyhedron_to_stl.hpp"
#include "meshrrill/polyhedron/ImageToPolyhedron.hpp"
#include "meshrrill/polyhedron/elastic_smooth_polyhedron.hpp"
#include "meshrrill/polyhedron/flatten_sharp_points.hpp"
#include "meshrrill/polyhedron/merge_small_edges.hpp"

#include <CGAL/Subdivision_method_3/subdivision_methods_3.h>

#include <fstream>


namespace meshrrill {


std::shared_ptr<ImageManager::Base::OptionsParser>
ImageManager::new_parser() const {
    return std::make_shared<OptionsParser>();
}


std::shared_ptr<ImageManager::Base::Mesher> ImageManager::new_mesher(
    const boost::program_options::variables_map& vm) const {
    const std::string filename = vm["image"].as<std::string>();
    const double vx            = vm["-X"].as<double>();
    const double vy            = vm["-Y"].as<double>();
    const double vz            = vm["-Z"].as<double>();

    double smooth = 1;
    if(vm.count("smooth") > 0) {
        smooth = vm["smooth"].as<double>();
    }

    std::shared_ptr<CachedTIFFReader> tiff_reader =
        std::make_shared<CachedTIFFReader>(vx, vy, vz);
    std::cout << "Opening TIFF File: " << filename << std::endl;
    std::ifstream tiff_file(filename);
    try {
        tiff_reader->read(tiff_file);
    } catch(const char* e) {
        std::cout << "Error opening TIFF File:" << std::endl;
        std::cout << "    " << e << std::endl;
        throw;
    }

    using Polyhedron = Mesher::Polyhedron;
    Polyhedron polyhedron;
    using I2P = ImageToPolyhedron<Polyhedron::HDS, CachedTIFFReader>;
    I2P i2p(tiff_reader);

    std::cout << "Building initial surface from image..." << std::endl;
    polyhedron.delegate(i2p);

    std::cout << "- Vertices:  " << polyhedron.size_of_vertices() << std::endl
              << "- Halfedges: " << polyhedron.size_of_halfedges() << std::endl
              << "- Closed:    " << polyhedron.is_closed() << std::endl;

    std::cout << "- Done" << std::endl << std::endl;


    // Run a number of smoothing routines on the initial image reconstruction
    // to get a smooth grain.
    std::cout << "Smoothing reconstruction..." << std::endl;

    std::cout << "- Elastic smoothing..." << std::endl;
    // The primary global smoothing routine to get a smooth grain from voxel
    // data. Tries to get rid of corners by minimizing the distance between
    // nodes, mimicing an elastic skin, but limiting their movement to a maximum
    // of 1 voxel to retain the initial shape.
    elastic_smooth_polyhedron(
        polyhedron, vx * smooth, vy * smooth, vz * smooth);

    std::cout << "- Merging small edges..." << std::endl;
    // Gets rid of small, near-degenerate edges that can result from the elastic
    // smoothing.
    merge_small_edges(polyhedron, CGAL::min(vx, CGAL::min(vy, vz)) * 0.7);

    std::cout << "- Flattening sharp edges..." << std::endl;
    // Gets rid of sharp points that can occur after elastic smoothing.
    for(int i = 0; i < 3; i++) {
        flatten_sharp_points(polyhedron, 359 * (2 * 3.14159265 / 360.0));
    }

    std::cout << "- Subdividing / smoothing..." << std::endl;
    // Increases the resolution of the surface with some local smoothing of
    // nodes and edges.
    // Good for avoiding surface artefacts during volume meshing.
    CGAL::Subdivision_method_3::Sqrt3_subdivision(polyhedron, 1);

    std::cout << "- Done" << std::endl << std::endl;

    bool output_surface = false;
    if(vm.count("surface") != 0u) {
        output_surface = vm["surface"].as<bool>();
    }

    // Output surface mesh to mesh file and return early.
    if(output_surface) {
        std::string output_filename("mesh.stl");

        if(vm.count("output") > 0) {
            output_filename = vm["output"].as<std::string>();
        }

        std::ofstream stl_file(output_filename.c_str());

        std::cout << "Writing surface to file..." << std::endl;
        output_polyhedron_to_stl(stl_file, polyhedron);
        std::cout << "- Done." << std::endl;

        // Return empty mesher: don't make volume mesh.
        return std::shared_ptr<Mesher>();
    }

    // Return polyhedron mesher with no sharp angles for volume meshing.
    const double cell_size = vm["cell-size"].as<double>();
    return std::make_shared<Mesher>(polyhedron, cell_size, 361.0);
}


} // namespace meshrrill
