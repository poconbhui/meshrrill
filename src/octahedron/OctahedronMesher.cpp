#include "meshrrill/octahedron/OctahedronMesher.hpp"

#include "meshrrill/octahedron/Octahedron.hpp"


namespace meshrrill {


OctahedronMesher::OctahedronMesher(
    double len_x, double len_y, double len_z, double truncation,
    double cell_size) :
    PolyhedronMesher(
        Octahedron<Polyhedron>(len_x, len_y, len_z, truncation), cell_size,
        10.0) {}

} // namespace meshrrill
