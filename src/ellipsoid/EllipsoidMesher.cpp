#include "meshrrill/ellipsoid/EllipsoidMesher.hpp"

#include "meshrrill/geometry/MeshDomainWithFeatures3Wrap.hpp"

#include <CGAL/Implicit_mesh_domain_3.h>
#include <CGAL/Labeled_mesh_domain_3.h>
#include <CGAL/Mesh_domain_with_polyline_features_3.h>


namespace meshrrill {


namespace {


EllipsoidMesher::FT in_ellipsoid(
    const EllipsoidMesher::Point& point, EllipsoidMesher::FT a2_inv,
    EllipsoidMesher::FT b2_inv, EllipsoidMesher::FT c2_inv) {
    return (
        std::pow(point.x(), 2) * a2_inv + std::pow(point.y(), 2) * b2_inv
        + std::pow(point.z(), 2) * c2_inv - 1.0);
    return 0;
}


} // namespace


EllipsoidMesher::EllipsoidMesher(
    double a, double b, double c, double cell_size) :
    ImplicitMesher(
        std::bind(
            in_ellipsoid, std::placeholders::_1, 1.0 / (a * a), 1.0 / (b * b),
            1.0 / (c * c)),
        Kernel::Sphere_3(
            CGAL::ORIGIN, 2 * CGAL::max(a * a, CGAL::max(b * b, c * c))),
        cell_size, cell_size * 1e-7) {}


} // namespace meshrrill
