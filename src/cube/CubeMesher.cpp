#include "meshrrill/cube/CubeMesher.hpp"

#include "meshrrill/cube/Cube.hpp"


namespace meshrrill {

CubeMesher::CubeMesher(
    double len_x, double len_y, double len_z, double cell_size) :
    PolyhedronMesher(Cube<Polyhedron>(len_x, len_y, len_z), cell_size) {}

} // namespace meshrrill
