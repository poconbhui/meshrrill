#include "meshrrill/polyhedron/PolyhedronMesher.hpp"
#include "compute_bounding_sphere.hpp"

#include <cmath>

#include <CGAL/Labeled_mesh_domain_3.h>

#include <CGAL/Polygon_mesh_processing/compute_normal.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>


namespace meshrrill {


// Triangulate the faces of the input polyhedron (if not already triangular)
// because Side_of_triangle_mesh expects a triangular polyhedron.
template<typename Polyhedron>
static Polyhedron triangulated_polyhedron(Polyhedron polyhedron) {
    CGAL::Polygon_mesh_processing::triangulate_faces(polyhedron); // NOLINT
    return polyhedron;
}

static double rad_to_deg(double angle_rad) {
    static double conversion = 180.0 / std::acos(-1);
    return conversion * angle_rad;
}


namespace {


class SideOfPolyhedron {
public:
    using Polyhedron = PolyhedronMesher::Polyhedron;
    using Kernel     = PolyhedronMesher::Kernel;
    using FT         = PolyhedronMesher::FT;
    using Point      = PolyhedronMesher::Point;
    using Sphere     = PolyhedronMesher::Kernel::Sphere_3;

    using SideOfTriangleMesh = CGAL::Side_of_triangle_mesh<Polyhedron, Kernel>;

    using return_type = int;
    using Point_3     = Point;

    Polyhedron polyhedron;
    SideOfTriangleMesh side_of_triangle_mesh;

    explicit SideOfPolyhedron(const Polyhedron& polyhedron_in) :
        polyhedron(triangulated_polyhedron(polyhedron_in)),
        side_of_triangle_mesh(polyhedron) {}

    FT operator()(const Point_3& point) const {
        CGAL::Bounded_side side = side_of_triangle_mesh(point); // NOLINT

        if(side == CGAL::ON_BOUNDED_SIDE) {
            return -1;
        }
        if(side == CGAL::ON_BOUNDARY) {
            return 0;
        }

        // ON_UNBOUNDED_SIDE
        return 1;
    }
};


} // namespace


PolyhedronMesher::PolyhedronMesher(
    const Polyhedron& polyhedron, double cell_size,
    double detected_angle_bound) :
    ImplicitMesher(
        std::bind(
            &SideOfPolyhedron::operator(),
            std::make_shared<SideOfPolyhedron>(polyhedron),
            std::placeholders::_1),
        compute_bounding_sphere<Kernel>(
            polyhedron.points_begin(), polyhedron.points_end()),
        cell_size, cell_size * 1e-4) {
    // Get a list of unique edges in the polyhedron
    // Use the untriangulated input polyhedron to avoid adding
    // unnecessary extra edges.
    using VertexHandle        = Polyhedron::Vertex_const_handle;
    using VertexHandlePair    = std::pair<VertexHandle, VertexHandle>;
    using VertexHandlePairSet = std::set<VertexHandlePair>;

    VertexHandlePairSet vertex_handle_pair_set;

    //
    // Const cast needed because compute_facet_normal only accepts
    // the non-const Facet_handle, not Facet_const_handle because
    // they're completely different types.
    // As a result, the function is const-incorrect.
    // The function doesn't set anything on the facet, so it should
    // be fine.
    //
    Polyhedron& polyhedron_in = const_cast<Polyhedron&>(polyhedron); // NOLINT

    using Hit = Polyhedron::Halfedge_iterator;
    for(Hit hit = polyhedron_in.halfedges_begin();
        hit != polyhedron_in.halfedges_end(); hit++) {
        // Add this edge if the angle between the incident facets is
        // smaller than the requested angle_bound.

        //
        // Get the normals of the two incident facets
        //
        using Vector_3 = Polyhedron::Traits::Vector_3;
        Vector_3 n1    = CGAL::Polygon_mesh_processing::compute_face_normal(
            hit->facet(), polyhedron_in);
        Vector_3 n2 = CGAL::Polygon_mesh_processing::compute_face_normal(
            hit->opposite()->facet(), polyhedron_in);


        // Use dot product to find angle between the facets
        double dot = n1 * n2;
        // Careful of rounding errors....
        if(dot > 1.0) {
            dot = 1.0;
        } else if(dot < -1.0) {
            dot = -1.0;
        }
        double angle = rad_to_deg(std::abs(std::acos(dot)));

        // Add this edge to the set if the angle is large enough.
        if(angle > detected_angle_bound) {
            VertexHandle v1 = hit->vertex();
            VertexHandle v2 = hit->opposite()->vertex();

            if(v1 > v2) {
                vertex_handle_pair_set.insert({v1, v2});
            } else {
                vertex_handle_pair_set.insert({v2, v1});
            }
        }
    }


    // Convert the set of unique edges to polylines
    using Point     = Polyhedron::Point_3;
    using Polyline  = std::vector<Point>;
    using Polylines = std::vector<Polyline>;

    Polylines polylines;
    polylines.reserve(vertex_handle_pair_set.size());
    for(const auto& vhsp_it : vertex_handle_pair_set) {
        polylines.push_back({vhsp_it.first->point(), vhsp_it.second->point()});
    }


    std::dynamic_pointer_cast<ImplicitMesher::MeshDomain>(implicit_domain)
        ->add_features(polylines.begin(), polylines.end());
}

} // namespace meshrrill
