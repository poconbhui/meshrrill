#include "meshrrill/geometry/GeometryMesher.hpp"

#include <CGAL/make_mesh_3.h>


namespace meshrrill {


namespace param = CGAL::parameters;


std::shared_ptr<GeometryMesher::Mesh> GeometryMesher::mesh() const {
    return std::make_shared<Mesh>();
}


std::shared_ptr<GeometryMesher::Mesh> GeometryMesher::make_mesh() const {
    std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();
    make_mesh(*mesh, *(mesh_domain()), *(mesh_criteria()));

    return mesh;
}


void GeometryMesher::make_mesh(
    Mesh& mesh, const MeshDomain& mesh_domain,
    const MeshCriteria& mesh_criteria) const {

    // Mesh generation
    // Mesh is a CGAL MeshComplex_3InTriangulation_3 concept.
    mesh = CGAL::make_mesh_3<Mesh>(
        mesh_domain, mesh_criteria, param::no_lloyd(), param::no_odt(),
        param::no_perturb(), param::no_exude());
}


void GeometryMesher::optimize_mesh(Mesh& mesh) const {
    optimize_mesh(mesh, *(mesh_domain()));
}


void GeometryMesher::optimize_mesh(
    Mesh& mesh, const MeshDomain& mesh_domain) const {
    // Optimize mesh
    CGAL::lloyd_optimize_mesh_3(mesh, mesh_domain);
    CGAL::odt_optimize_mesh_3(mesh, mesh_domain);
    CGAL::perturb_mesh_3(mesh, mesh_domain);
    CGAL::exude_mesh_3(mesh);
}


} // namespace meshrrill
