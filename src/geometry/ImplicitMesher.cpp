#include "meshrrill/geometry/ImplicitMesher.hpp"

#include "meshrrill/geometry/MeshDomainWithFeatures3Wrap.hpp"

#include <CGAL/Labeled_mesh_domain_3.h>

#include <utility>


namespace meshrrill {


namespace {


template<typename Type>
class Holder {
public:
    Type object;
    explicit Holder(Type object) : object(std::move(object)) {}
};


class ImplicitMeshDomain
    : public Holder<ImplicitMesher::Function>,
      public MeshDomainWithFeatures3Wrap<
          CGAL::Mesh_domain_with_polyline_features_3<
              CGAL::Implicit_mesh_domain_3<
                  ImplicitMesher::Function, ImplicitMesher::Kernel>>> {
public:
    using FunctionHolder = Holder<ImplicitMesher::Function>;
    using Base           = MeshDomainWithFeatures3Wrap<
        CGAL::Mesh_domain_with_polyline_features_3<CGAL::Implicit_mesh_domain_3<
            ImplicitMesher::Function, ImplicitMesher::Kernel>>>;

    ImplicitMeshDomain(
        ImplicitMesher::Function function,
        ImplicitMesher::Kernel::Sphere_3 bounding_sphere,
        ImplicitMesher::Kernel::FT error_bound) :
        FunctionHolder(std::move(function)),
        Base(this->FunctionHolder::object, bounding_sphere, error_bound) {}
};


} // namespace


ImplicitMesher::ImplicitMesher(
    const Function& function, Sphere bounding_sphere, FT cell_size,
    FT error_bound) :

    implicit_domain(std::make_shared<ImplicitMeshDomain>(
        function, bounding_sphere, error_bound)),

    cell_size(cell_size) {}


std::shared_ptr<GeometryMesher::MeshDomain>
ImplicitMesher::mesh_domain() const {
    return implicit_domain;
}


std::shared_ptr<GeometryMesher::MeshCriteria>
ImplicitMesher::mesh_criteria() const {
    // Equilateral tetrahedron circumsphere radius to edge length:
    // r = (e/4)*sqrt(6)
    // r*(4/sqrt(6)) = e

    // Mesh criteria
    FT edge_size = cell_size * (4.0 / sqrt(6));

    FT facet_angle    = 30;
    FT facet_size     = cell_size;
    FT facet_distance = cell_size / 10.0;

    FT cell_radius_edge_ratio = 2.0;

    return std::make_shared<MeshCriteria>(
        // std::make_shared<MeshCriteria::Edge_criteria>(cell_size),
        std::make_shared<MeshEdgeCriteria_Wrap<
            MeshCriteria::Triangulation,
            CGAL::Mesh_edge_criteria_3<MeshCriteria::Triangulation>>>(
            edge_size),
        std::make_shared<MeshFacetCriteria_Wrap<
            MeshCriteria::Triangulation,
            CGAL::Mesh_facet_criteria_3<MeshCriteria::Triangulation>>>(
            facet_angle, facet_size, facet_distance),
        std::make_shared<MeshCellCriteria_Wrap<
            MeshCriteria::Triangulation,
            CGAL::Mesh_cell_criteria_3<MeshCriteria::Triangulation>>>(
            cell_radius_edge_ratio, cell_size));
}


} // namespace meshrrill
