#include "meshrrill/io/CachedTIFFReader.hpp"

#include <tiff.h>
#include <tiffio.h>
#include <tiffio.hxx>

#include <algorithm>
#include <cassert>

namespace meshrrill {

using FT = CachedTIFFReader::FT;
CachedTIFFReader::CachedTIFFReader(const FT vx, const FT vy, const FT vz) :
    nx(), ny(), nz(), vx(vx), vy(vy), vz(vz), tiff(nullptr), raster_cache(3) {
    for(auto& rc_it : raster_cache) {
        rc_it.first = -1;
    }
}

CachedTIFFReader::~CachedTIFFReader() {
    if(tiff != nullptr) {
        TIFFClose(tiff);
    }
    tiff = nullptr;
}


void CachedTIFFReader::read(std::istream& istream) {
    TIFFErrorHandler old_handler = TIFFSetWarningHandler(nullptr);

    // Open TIFF stream and check it's ok.
    tiff = TIFFStreamOpen("TIFFStackToSurface::tiff", &istream);
    if(tiff == nullptr) {
        throw Exception("TIFFStackToSurface::read: Unable to open TIFF File");
    }

    // Get height and width of first image, and get number of images.
    TIFFSetDirectory(tiff, 0);
    // TIFFGetField is a variadic function. There are no alternatives calls,
    // so we suppress the cppcoreguidelines warnings.
    TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &nx);  // NOLINT
    TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &ny); // NOLINT
    for(nz = 1; TIFFReadDirectory(tiff) != 0; nz++) {
        // Do Nothing.
        // TIFFReadDirectory ticks the current directory of tiff and
        // returns false after the last directory.
    }


    // Setup raster layers
    std::size_t npix_per_layer = nx * ny;
    for(auto& rc_it : raster_cache) {
        rc_it.first = -1;
        rc_it.second.resize(npix_per_layer);

        std::fill(std::begin(rc_it.second), std::end(rc_it.second), 0);
    }

    TIFFSetWarningHandler(old_handler);
}

void CachedTIFFReader::read_layer(const std::size_t z, Raster& raster) {
    TIFFErrorHandler old_handler = TIFFSetWarningHandler(nullptr);

    assert(raster != NULL);

    if(z < nz) {
        TIFFSetDirectory(tiff, z);
        TIFFReadRGBAImage(tiff, nx, ny, raster.data(), 0);
    } else {
        std::fill(std::begin(raster), std::end(raster), 0);
    }

    TIFFSetWarningHandler(old_handler);
}

const CachedTIFFReader::Raster&
CachedTIFFReader::get_cached_layer(const std::size_t z) {
    auto target_it = raster_cache.end();

    // Find iterator for the cached layer
    for(auto rc_it = raster_cache.begin(); rc_it != raster_cache.end();
        rc_it++) {
        if(rc_it->first == z) {
            target_it = rc_it;
        }
    }

    if(target_it != raster_cache.end()) {
        // If found, move the iterator to the front of the array
        // and return the Raster
        std::rotate(raster_cache.begin(), target_it, next(target_it));
        target_it = raster_cache.begin();

        return target_it->second;
    }

    // If not found, read the raster layer into the back of
    // the array and run this method again to look for it.
    RasterCache::reference cache = raster_cache.back();
    cache.first                  = z;
    read_layer(z, cache.second);

    return get_cached_layer(z);
}


std::uint32_t
CachedTIFFReader::value(std::size_t x, std::size_t y, std::size_t z) {
    assert(x < nx);
    assert(y < ny);
    const Raster& raster_layer = get_cached_layer(z);

    assert(y * nx + x < nx * ny);
    return TIFFGetR(raster_layer[y * nx + x]);
}

} // namespace meshrrill
