# MEshRRILL

MEshRRILL is an easy-to-use meshing tool for generating tetrahedral meshed
geometries in common use in Rock Magnetism, intended as a companion for MERRILL.

Download the latest version (1.0.1) here:

- [macOS
  10.10](https://bitbucket.org/poconbhui/meshrrill/downloads/meshrrill-1.0.1-OSX-10.10-64bit.tar.gz)
- [Linux](https://bitbucket.org/poconbhui/meshrrill/downloads/meshrrill-1.0.1-Linux-64bit.tar.gz)


As a note, MEshRRILL meshes don't actually include any dimensions,
i.e. a mesh of width 0.1 is just 0.1, not m, um, or nm.
However, MERRILL, by default, assumes 1 mesh unit is 1 um.
In the rest of this README, we will therefore refer to 1 unit as 1 um.

If you're having difficulties using MEshRRILL, or would like a geometry added,
feel free to [add an issue in the issue tracker](https://bitbucket.org/poconbhui/meshrrill/issues)!
MEshRRILL aims to be an easy-to-use, comprehensive solution to meshing for
the rock magnetic community.


##Quick Start


### Cube

Generate a `0.100 um` (i.e. `100 nm`) cube with cell size `0.005 um` (i.e. 5 nm) to the file `cube.neu`:

    meshrrill cube 0.1 0.1 0.1 -c 0.005 -o cube.neu


### Sphere

Generate a `0.080 um` sphere with cell size `0.0035 um` to the file `sphere.neu`:

    meshrrill ellipsoid 0.08 0.08 0.08 -c 0.0035 -o sphere.neu


### Truncated Octahedron

Generate a `0.120 um` truncated octahedron with truncation factor `0.1`, cell size `0.005 um` to the default file name `mesh.neu`:

    meshrrill octahedron 0.12 0.12 0.12 -t 0.1 -c 0.005


### Reconstructed TIFF Stack 

Generate a smoothed and meshed reconstruction of a TIFF stack image from the file `my_tiff_stack.tif` with voxel size `0.001 x 0.002 x 0.003 um^3` and mesh size `0.0035 um` to the default file name `mesh.neu`:

    meshrrill image my_tiff_stack.tif 0.001 0.002 0.003 -c 0.0035


### Finding Help

For more information on what geometries/commands are available, try

    meshrrill --help

For more information on a particular geometry, e.g. the `cube` command, try

    meshrrill cube --help


## TIFF Stack Reconstruction

MEshRRILL can be used to reconstruct a geometry from a tomography stack,
like the results obtained from FIB nanotomography.

To reconstruct and mesh a tiff stack called, say, `my_tiff_stack.tif`, with a
voxel size of `0.1x0.2x0.3` to a tetrahedron size of `0.4`, run

    meshrrill image my_tiff_stack.tif 0.1 0.2 0.3 --cell-size 0.4

To output the file to a different name, use the `--output` option:

    meshrrill image my_tiff_stack.tif 0.1 0.2 0.3 --cell-size 0.4 --output my_mesh.neu

If you want to output the reconstructed surface used to generate the mesh, use the `--surface` option:

    meshrrill image my_tiff_stack.tif 0.1 0.2 0.3 --cell-size 0.4 --surface

The smoothing used for the reconstruction is managed by the `--smooth` option.
The smoothing works by treating the surface a bit like a contracting rubber
sheet, but constrains the original points (i.e.  corners of the original blocky
image) to move at most `s/2` voxels from their starting position, where `s` is
the smoothing factor.
A smoothing factor up to 1 (the default used) should produce results perfectly
justifiable by the original image.
Beyond that may be extra smoothing.
For a smoothing of 2, for example:

    meshrrill image my_tiff_stack.tif 0.1 0.2 0.3 --cell-size 0.4 --smooth 2

and all together, for example:

    meshrrill image my_tiff_stack.tif 0.1 0.2 0.3 --cell-size 0.4 --smooth 2 --surface --output my_mesh.stl
