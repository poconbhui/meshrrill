#!/usr/bin/env bash

set -e

# Get the current script directory
# Looks a bit funky so it works on Mac and Linux.
script_dir="$(cd "$(dirname "$(which ${BASH_SOURCE[0]})")"; echo $PWD)"

# Expect demo to be in meshrrill_install_root/share/meshrrill/demo/xxxdir/
: ${MEshRRILL_INSTALL_ROOT=${script_dir}/../../../../}

# Config files installed in meshrrill_root/lib/meshrrill
: ${MEshRRILL_CONFIG_DIR=${MEshRRILL_INSTALL_ROOT}/lib/meshrrill}


# Set up useful directories
source_dir=$script_dir
: ${demo_build_dir=$PWD/build/build}
: ${demo_install_dir=$PWD/build/install}


# Make the build directory and cd into it
mkdir -p $demo_build_dir

# Run CMake to configure the project
cmake \
    -B$demo_build_dir -H$source_dir \
    -DCMAKE_INSTALL_PREFIX:PATH="$demo_install_dir" \
    -DMEshRRILL_DIR:PATH="$MEshRRILL_CONFIG_DIR" \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    "$@"

# Make and install the project
cmake --build $demo_build_dir
cmake --build $demo_build_dir --target install

export LD_LIBRARY_PATH=${MEshRRILL_INSTALL_ROOT}/lib:${LD_LIBRARY_PATH}
unzip -n "${source_dir}/DO20140814E realigned stack 7.tif.zip"

time ${demo_install_dir}/bin/fib_stack \
    "DO20140814E realigned stack 7.tif" \
    $(echo 0.009766  / 2 | bc -l) \
    $(echo 0.0132034 / 2 | bc -l) \
    $(echo 0.020     / 2 | bc -l) \
    0.0032
