#include <CGAL/Mesh_3/global_parameters.h>
#include <CGAL/squared_distance_3.h>
#include <CGAL/squared_distance_2.h>
#include <meshrrill/polyhedron/merge_small_edges.hpp>
#include <meshrrill/polyhedron/flatten_sharp_points.hpp>
#include <meshrrill/polyhedron/elastic_smooth_polyhedron.hpp>
#include <meshrrill/polyhedron/ImageToPolyhedron.hpp>
#include <meshrrill/io/CachedTIFFReader.hpp>
#include <meshrrill/polyhedron/PolyhedronMesher.hpp>
#include <meshrrill/io/output_to_patran.hpp>

#include <iostream>
#include <algorithm>

#include <fstream>


#include <meshrrill/geometry/Kernel.hpp>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/HalfedgeDS_decorator.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/triangulate_hole.h>
#include <CGAL/Polygon_mesh_processing/connected_components.h>
#include <CGAL/Aff_transformation_3.h>
#include <CGAL/aff_transformation_tags.h>
#include <CGAL/Subdivision_method_3/subdivision_methods_3.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/jet_smooth_point_set.h>
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>

#include "output_polyhedron_to_stl.h"

namespace meshrrill {


//
// Build a polyhedron from an iterator of facets
//
template<typename HDS, typename FacetIterator>
class PolyhedronFromFacetsModifier: public CGAL::Modifier_base<HDS> {
public:
    const FacetIterator facets_begin;
    const FacetIterator facets_end;

    PolyhedronFromFacetsModifier(
        FacetIterator facets_begin, FacetIterator facets_end
    ):
        facets_begin(facets_begin),
        facets_end(facets_end)
    {}

    void operator()(HDS& hds) {
        typedef typename HDS::Vertex_handle Vertex_handle;
        typedef typename HDS::Halfedge_handle Halfedge_handle;

        typedef std::map<Vertex_handle, std::size_t> VertexIdMap;
        VertexIdMap vertex_id_map;


        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);
        B.begin_surface(0, 0, 0);


        for(FacetIterator fit = facets_begin; fit != facets_end; fit++) {
            Halfedge_handle h = (*fit)->halfedge();
            const Halfedge_handle h_end = h;

            B.begin_facet();
            do {
                typedef typename VertexIdMap::iterator VertexIdMapIterator;
                typedef std::pair<VertexIdMapIterator, bool> InsertReturn;
                InsertReturn ir = vertex_id_map.insert(
                    typename VertexIdMap::value_type(
                        h->vertex(), vertex_id_map.size()
                    )
                );
                if(ir.second) B.add_vertex(ir.first->first->point());

                B.add_vertex_to_facet(ir.first->second);

                h = h->next();
            } while(h != h_end);
            B.end_facet();
        }

        B.end_surface();
    }
};



} // namespace meshrrill



int main(int argc, char* argv[]) {

    using namespace meshrrill;

    using Polyhedron = PolyhedronMesher::Polyhedron;


    if(argc != 6) {
        std::cout << "Usage: " << argv[0] << "stack_file.tif vx vy vz cell_size" << std::endl;
        return EXIT_FAILURE;
    }

    std::string stack_filename = argv[1];
    PolyhedronMesher::Kernel::FT vx = std::stof(argv[2]);
    PolyhedronMesher::Kernel::FT vy = std::stof(argv[3]);
    PolyhedronMesher::Kernel::FT vz = std::stof(argv[4]);

    PolyhedronMesher::Kernel::FT cell_size = std::stof(argv[5]);


    std::shared_ptr<CachedTIFFReader> tiff_reader =
        std::make_shared<CachedTIFFReader>(vx, vy, vz);
    std::ifstream tiff_file(stack_filename);
    std::cout << "main 1" << std::endl;
    try {
        tiff_reader->read(tiff_file);
    } catch (const char* e) {
        std::cout << "EX: " << e << std::endl;
    }
    std::cout << "main 2" << std::endl;

    Polyhedron polyhedron;
    typedef ImageToPolyhedron<Polyhedron::HDS, CachedTIFFReader> I2P;
    I2P i2p(tiff_reader);
    polyhedron.delegate(i2p);

    std::cout
        << "polyhedron.size_of_vertices "
        << polyhedron.size_of_vertices()
        << std::endl;
    std::cout
        << "polyhedron.size_of_halfedges "
        << polyhedron.size_of_halfedges()
        << std::endl;
    std::cout
        << "polyhedron.is_closed() "
        << polyhedron.is_closed()
        << std::endl;

    std::ofstream full_stl_file("full.stl");
    output_polyhedron_to_stl(full_stl_file, polyhedron);
    full_stl_file.flush();


    std::cout << "main: Splitting meshes" << std::endl;

    std::cout << "main: Finding facet closest to center" << std::endl;
    Polyhedron::Point p(
        tiff_reader->nx*tiff_reader->vx,
        tiff_reader->ny*tiff_reader->vy,
        tiff_reader->nz*tiff_reader->vz
    );
    Polyhedron::Vertex_handle candidate_vertex = polyhedron.vertices_begin();
    for(
        Polyhedron::Vertex_iterator vit = polyhedron.vertices_begin();
        vit != polyhedron.vertices_end();
        vit++
    ) {
        if(
            CGAL::squared_distance(p, vit->point())
                < CGAL::squared_distance(p, candidate_vertex->point())
        ) {
            candidate_vertex = vit;
        }
    }

    std::cout << "Candidate vertex: "
        << candidate_vertex->point().x() << ", "
        << candidate_vertex->point().y() << ", "
        << candidate_vertex->point().z() << std::endl;

    Polyhedron::Facet_handle candidate_facet =
        candidate_vertex->halfedge()->facet();

    std::cout << "Finding connected faces" << std::endl;
    typedef std::vector<Polyhedron::Facet_handle> ConnectedFaces;
    ConnectedFaces connected_faces;
    CGAL::Polygon_mesh_processing::connected_component(
        candidate_facet,
        polyhedron,
        std::back_inserter(connected_faces)
    );

    std::cout << "n connected faces: " << connected_faces.size() << std::endl;

    PolyhedronFromFacetsModifier<Polyhedron::HalfedgeDS, ConnectedFaces::iterator>
        polyhedron_from_facets_modifier(
            connected_faces.begin(), connected_faces.end()
        );
    Polyhedron sub_polyhedron;
    sub_polyhedron.delegate(polyhedron_from_facets_modifier);

    std::cout << "Finding polyhedron width" << std::endl;
    auto xmax = std::numeric_limits<PolyhedronMesher::FT>::min();
    auto xmin = std::numeric_limits<PolyhedronMesher::FT>::max();
    auto ymax = std::numeric_limits<PolyhedronMesher::FT>::min();
    auto ymin = std::numeric_limits<PolyhedronMesher::FT>::max();
    auto zmax = std::numeric_limits<PolyhedronMesher::FT>::min();
    auto zmin = std::numeric_limits<PolyhedronMesher::FT>::max();
    for(
        auto p_it = sub_polyhedron.points_begin();
        p_it != sub_polyhedron.points_end();
        p_it++
    ) {
        xmax = std::max(xmax, p_it->x());
        xmin = std::min(xmin, p_it->x());

        ymax = std::max(ymax, p_it->y());
        ymin = std::min(ymin, p_it->y());

        zmax = std::max(zmax, p_it->z());
        zmin = std::min(zmin, p_it->z());
    }
    std::cout << "    x: " << xmax - xmin << std::endl;
    std::cout << "    y: " << ymax - ymin << std::endl;
    std::cout << "    z: " << zmax - zmin << std::endl;

    std::ofstream original_stl_file("original.stl");
    output_polyhedron_to_stl(original_stl_file, sub_polyhedron);

    elastic_smooth_polyhedron(
        sub_polyhedron, vx, vy, vz
    );
    std::cout << "main: Outputting stl" << std::endl;
    std::ofstream elastic_smooth_file("elastic_smooth.stl");
    output_polyhedron_to_stl(elastic_smooth_file, sub_polyhedron);

    merge_small_edges(
        sub_polyhedron, CGAL::min(vx, CGAL::min(vy, vz))/2
    );
    std::cout << "Outputting simplified" << std::endl;
    std::ofstream simplified_file("simplified.stl");
    output_polyhedron_to_stl(simplified_file, sub_polyhedron);
    simplified_file.flush();

    flatten_sharp_points(
        sub_polyhedron, 359 * (2*3.14159265/360.0)
    );
    flatten_sharp_points(
        sub_polyhedron, 359 * (2*3.14159265/360.0)
    );
    flatten_sharp_points(
        sub_polyhedron, 359 * (2*3.14159265/360.0)
    );
    std::ofstream flatten_smooth_file("flatten_smooth.stl");
    output_polyhedron_to_stl(flatten_smooth_file, sub_polyhedron);

    //const unsigned int nb_neighbours = 24;
    //CGAL::jet_smooth_point_set<CGAL::Sequential_tag>(
    //    polyhedron.points_begin(), polyhedron.points_end(),
    //    nb_neighbours
    //);
    //std::ofstream jet_smooth_file("jet_smooth.stl");
    //output_polyhedron_to_stl(jet_smooth_file, sub_polyhedron);

    ////CGAL::Subdivision_method_3::CatmullClark_subdivision(
    ////CGAL::Subdivision_method_3::Loop_subdivision(
    ////CGAL::Subdivision_method_3::DooSabin_subdivision(

    ////output_polyhedron_to_stl(stl_file, tiff_reader.polyhedron);
    //CGAL::Polygon_mesh_processing::triangulate_faces(sub_polyhedron);
    //std::ofstream cgal_smooth_file("cgal_smooth.stl");
    //output_polyhedron_to_stl(cgal_smooth_file, sub_polyhedron);

    auto mesher = PolyhedronMesher(sub_polyhedron, cell_size, 361.0);
    auto mesh = mesher.make_mesh();
    mesher.optimize_mesh(*mesh);

    std::cout
        << "Mesh stats:" << std::endl
        << "    nodes: " << mesh->number_of_cells() << std::endl
        << "    tets:  " << mesh->triangulation().number_of_vertices() << std::endl;


    auto patran_file = std::ofstream("mesh.neu");
    output_mesh_to_patran(patran_file, *mesh);

    std::cout << "Done" << std::endl;

    return EXIT_SUCCESS;
}
