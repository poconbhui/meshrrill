#include <meshrrill/multidomain/MultiDomainMesher.hpp>

#include <meshrrill/octahedron/OctahedronMesher.hpp>
#include <meshrrill/ellipsoid/EllipsoidMesher.hpp>

#include <meshrrill/io/output_to_patran.hpp>

int main(int argc, char* argv[]) {

    using namespace meshrrill;

    double octahedron_size = 0.1;
    double octahedron_truncation = 0.1;
    OctahedronMesher octahedron(
        octahedron_size, octahedron_size, octahedron_size,
        octahedron_truncation,
        0.005
    );

    double inner_sphere_radius = 0.05;
    EllipsoidMesher inner_sphere(
        inner_sphere_radius, inner_sphere_radius, inner_sphere_radius,
        inner_sphere_radius/5
    );

    double outer_sphere_radius = 0.07;
    EllipsoidMesher outer_sphere(
        outer_sphere_radius, outer_sphere_radius, outer_sphere_radius,
        (outer_sphere_radius - inner_sphere_radius)/5
    );


    MultiDomainMesher multidomain_mesher(
        MultiDomainMesher::Kernel::Sphere_3(CGAL::ORIGIN, 0.3),
        octahedron_size*1e-4
    );

    multidomain_mesher.add(octahedron, true);
    multidomain_mesher.add(inner_sphere);
    multidomain_mesher.add(outer_sphere);

    std::shared_ptr<MultiDomainMesher::Mesh> multidomain_mesh =
        multidomain_mesher.make_mesh();

    std::ofstream patran_file("multidomain.neu");
    output_mesh_to_patran(patran_file, *multidomain_mesh);

    return EXIT_SUCCESS;
}
