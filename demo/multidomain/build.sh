#!/usr/bin/env bash

# Get thr current script directory
# Looks a bit funky so it works on Mac and Linux.
script_dir="$(cd "$(dirname "$(which ${BASH_SOURCE[0]})")"; echo $PWD)"

# Expect demo to be in meshrrill_install_root/share/meshrrill/demo/xxxdir/
: ${MEshRRILL_INSTALL_ROOT=${script_dir}/../../../../}

# Config files installed in meshrrill_root/lib/meshrrill
: ${MEshRRILL_CONFIG_DIR=${MEshRRILL_INSTALL_ROOT}/lib/meshrrill}


# Set up useful directories
source_dir=$script_dir
: ${demo_build_dir=$PWD/build/build}
: ${demo_install_dir=$PWD/build/install}


# Make the build directory and cd into it
mkdir -p $demo_build_dir
cd $demo_build_dir

# Run CMake to configure the project
cmake \
    -DCMAKE_INSTALL_PREFIX:PATH="$demo_install_dir" \
    -DMEshRRILL_DIR:PATH="$MEshRRILL_CONFIG_DIR" \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    "$source_dir"

# Make and install the project
make
make install

# Run the project
# $install_dir/bin/multigrain
