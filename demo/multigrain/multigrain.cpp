#include <meshrrill/io/output_to_patran.hpp>

#include <iostream>

#include <meshrrill/octahedron/OctahedronMesher.hpp>
#include <meshrrill/geometry/Mesh.hpp>

int main(int argc, char* argv[]) {

    // Define some useful types

    // Mesh types
    typedef meshrrill::defaults::Mesh Mesh;

    // Mesh iterator
    typedef Mesh::MeshIterator MeshIterator;

    // Some point and connectivity types, storage and iterators
    typedef MeshIterator::Point         Point;
    typedef MeshIterator::PointIterator PointIterator;

    typedef MeshIterator::Connectivity         Connectivity;
    typedef MeshIterator::ConnectivityIterator ConnectivityIterator;

    // The list of points and connectivities we will populate to
    // make our final mesh.
    std::vector<Point> points;
    std::vector<Connectivity> connectivities;

    // Define some dimensions for the meshed geometries we'll generate.
    // We'll just use octahedra here.
    double octahedron_dims[3] = {1.0, 1.0, 1.0};
    double truncation = 0.2;
    double mesh_cell_size = 0.1;

    // Define some spacing between the octahedra.
    // We could place them in any arbitrary positions, but we'll just
    // set them out in a row for now.
    double spacing[3] = {1.5, 0.0, 0.0};

    // Loop over the octahedra we want to generate.
    for(std::size_t i=0; i<5; i++) {

        // Generate a mesher which makes an octahedron.
        meshrrill::OctahedronMesher octahedron_mesher(
            octahedron_dims[0], octahedron_dims[1], octahedron_dims[2],
            truncation,
            mesh_cell_size
        );

        // Make the mesh
        std::shared_ptr<meshrrill::OctahedronMesher::Mesh> mesh =
            octahedron_mesher.make_mesh();

        // Optimize the mesh
        octahedron_mesher.optimize_mesh(*mesh);

        // Get a point/connectivity iterator for the mesh
        MeshIterator octahedron_it = mesh->get_mesh_iterator();


        // We need to define a connectivity offset because the connectivities
        // in octahedron_it will start at 0. We need them to start after the
        // last point inserted into points.
        std::size_t connectivity_offset = points.size();

        // Iterate over the points in the mesh
        for(
            PointIterator pit = octahedron_it.points_begin();
            pit != octahedron_it.points_end();
            pit++
        ) {
            // Get the point and translate it to wherever this octahedron
            // is supposed to be.
            Point p(
                pit->x() + i*spacing[0],
                pit->y() + i*spacing[1],
                pit->z() + i*spacing[2]
            );

            // Add the translated point to the global mesh points.
            points.push_back(p);
        }

        // Iterate over the connectivities
        for(
            ConnectivityIterator cit = octahedron_it.connectivities_begin();
            cit != octahedron_it.connectivities_end();
            cit++
        ) {
            // Add the offset to this connectivity, so it will actually
            // reference the points just added in the global mesh.
            Connectivity c;
            for(std::size_t i=0; i<4; i++) {
                c[i] = (*cit)[i] + connectivity_offset;
            }

            // Add the connectivities to the global mesh connectivities
            connectivities.push_back(c);
        }

    }

    // Open a new .neu file and write the mesh points and connectivities
    // to it.
    std::ofstream patran_file("multigrain_output.neu");
    meshrrill::output_to_patran(
        patran_file,
        points.begin(), points.end(),
        connectivities.begin(), connectivities.end()
    );

    return EXIT_SUCCESS;
}
