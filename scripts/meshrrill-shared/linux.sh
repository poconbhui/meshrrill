#!/usr/bin/env bash

script_dir="$(cd "$(dirname "$(which "${BASH_SOURCE[0]}")")"; echo $PWD)"

: ${DIST_ID:=Linux-64bit}
export DIST_ID

: ${BOOST_TOOLSET:=gcc}
export BOOST_TOOLSET

: ${CMAKE_C_COMPILER:=$(which gcc)}
: ${CMAKE_C_FLAGS:="-O3"}
export CMAKE_C_COMPILER
export CMAKE_C_FLAGS

: ${CMAKE_CXX_COMPILER:=$(which g++)}
: ${CMAKE_CXX_FLAGS:="-O3"}
export CMAKE_CXX_COMPILER
export CMAKE_CXX_FLAGS

: ${CMAKE_EXE_LINKER_FLAGS:=""}
export CMAKE_EXE_LINKER_FLAGS

: ${CMAKE_EXTRA_OPTIONS:=""}
export CMAKE_EXTRA_OPTIONS

: ${RUN_COMMON:=yes}

test x"$RUN_COMMON" = x"yes" && exec $script_dir/common.sh "$@"
