include(ExternalProject)


set(
    LIBTIFF_URL "http://dl.maptools.org/dl/libtiff/old/tiff-3.8.1.zip" CACHE STRING
    "URL to the libTIFF tarball."
)
set(
    LIBTIFF_MD5 "6c957bd0449d3f37d377a4b4881d5c85" CACHE STRING
    "MD5 hash of the libTIFF tarball."
)


set(LIBTIFF_PREFIX      "${CMAKE_BINARY_DIR}/libtiff" CACHE PATH "")
set(LIBTIFF_SOURCE_DIR  "${LIBTIFF_PREFIX}/src"       CACHE PATH "")
set(LIBTIFF_BUILD_DIR   "${LIBTIFF_PREFIX}/build"     CACHE PATH "")
set(LIBTIFF_INSTALL_DIR "${LIBTIFF_PREFIX}"           CACHE PATH "")

set(
    LIBTIFF_BUILD_EXTRA_OPTIONS "" CACHE PATH
    "Extra options to pass to the LibTIFF build command"
)


if(CMAKE_VERBOSE_MAKEFILE)
    set(LIBTIFF_VERBOSITY "--disable-silent-rules")
else()
    set(LIBTIFF_VERBOSITY "--enable-silent-rules")
endif()

if(BUILD_SHARED_LIBS)
    set(LIBTIFF_LINKAGE "--enable-shared;--disable-static")
else()
    set(LIBTIFF_LINKAGE "--disable-shared;--enable-static")
endif()

ExternalProject_Add(
    libtiff

    PREFIX ${LIBTIFF_PREFIX}

    SOURCE_DIR  ${LIBTIFF_SOURCE_DIR}
    BINARY_DIR  ${LIBTIFF_BUILD_DIR}
    INSTALL_DIR ${LIBTIFF_INSTALL_DIR}

    STAMP_DIR ${LIBTIFF_BUILD_DIR}

    URL ${LIBTIFF_URL}
    URL_MD5 ${LIBTIFF_MD5}

    PATCH_COMMAND
        ${CMAKE_COMMAND} -E remove ${LIBTIFF_SOURCE_DIR}/libtiff/tiffconf.h

    CONFIGURE_COMMAND
	${CMAKE_COMMAND} -E chdir ${LIBTIFF_BUILD_DIR}
            ${LIBTIFF_SOURCE_DIR}/configure
                ${LIBTIFF_VERBOSITY}
                "CC=${CMAKE_C_COMPILER}"
                "CFLAGS=${CMAKE_C_FLAGS} "
                "CXX=${CMAKE_CXX_COMPILER}"
                "CXXFLAGS=${CMAKE_CXX_FLAGS} "
                "--prefix=${LIBTIFF_INSTALL_DIR}"
                "--disable-jpeg"
                "--disable-old-jpeg"
                "--disable-zlib"
                "--enable-cxx"
                "--disable-mdi"
                "--without-x"
                "--disable-check-ycbcr-subsampling"
                ${LIBTIFF_LINKAGE}
                --disable-assembly
                --with-pic

    BUILD_COMMAND
        ${CMAKE_MAKE_PROGRAM}
            -C ${LIBTIFF_BUILD_DIR}
            ${LIBTIFF_BUILD_EXTRA_OPTIONS}

    INSTALL_COMMAND
        ${CMAKE_MAKE_PROGRAM}
            -C ${LIBTIFF_BUILD_DIR}
            install
)
