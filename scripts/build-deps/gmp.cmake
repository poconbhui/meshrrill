include(ExternalProject)


set(
    GMP_URL "https://gmplib.org/download/gmp/gmp-6.1.1.tar.bz2" CACHE STRING
    "URL to the GMP tarball."
)
set(
    GMP_MD5 "4c175f86e11eb32d8bf9872ca3a8e11d" CACHE STRING
    "MD5 hash of the GMP tarball."
)


option(GMP_ENABLE_TESTING "Test GMP build before installation" OFF)

if(GMP_ENABLE_TESTING)
    set(GMP_TEST_COMMAND "make check")
else()
    set(GMP_TEST_COMMAND "true")
endif(GMP_ENABLE_TESTING)


option(
    GMP_PORTABLE_BUILD
    "Build GMP so it's architecture independent."
    ON
)


set(GMP_PREFIX      "${CMAKE_BINARY_DIR}/gmp" CACHE PATH "")
set(GMP_SOURCE_DIR  "${GMP_PREFIX}/src"       CACHE PATH "")
set(GMP_BUILD_DIR   "${GMP_PREFIX}/build"     CACHE PATH "")
set(GMP_INSTALL_DIR "${GMP_PREFIX}"           CACHE PATH "")

set(
    GMP_BUILD_EXTRA_OPTIONS "" CACHE PATH
    "Extra options to pass to the GMP build command"
)


if(CMAKE_VERBOSE_MAKEFILE)
    set(GMP_VERBOSITY "--disable-silent-rules")
else()
    set(GMP_VERBOSITY "--enable-silent-rules")
endif()

if(BUILD_SHARED_LIBS)
    set(GMP_LINKAGE "--enable-shared;--disable-static")
else()
    set(GMP_LINKAGE "--disable-shared;--enable-static")
endif()

ExternalProject_Add(
    gmp

    PREFIX ${GMP_PREFIX}

    SOURCE_DIR  ${GMP_SOURCE_DIR}
    BINARY_DIR  ${GMP_BUILD_DIR}
    INSTALL_DIR ${GMP_INSTALL_DIR}

    STAMP_DIR ${GMP_BUILD_DIR}

    URL ${GMP_URL}
    URL_MD5 ${GMP_MD5}

    CONFIGURE_COMMAND
	${CMAKE_COMMAND} -E chdir ${GMP_BUILD_DIR}
            ${GMP_SOURCE_DIR}/configure
                ${GMP_VERBOSITY}
                "CC=${CMAKE_C_COMPILER}"
                "CFLAGS=${CMAKE_C_FLAGS} "
                "CXX=${CMAKE_CXX_COMPILER}"
                "CXXFLAGS=${CMAKE_CXX_FLAGS} "
                "--prefix=${GMP_INSTALL_DIR}"
                ${GMP_LINKAGE}
                --disable-assembly
                #--enable-fat
                --with-pic

    BUILD_COMMAND
        ${CMAKE_MAKE_PROGRAM}
            -C ${GMP_BUILD_DIR}
            ${GMP_BUILD_EXTRA_OPTIONS}

    TEST_BEFORE_INSTALL ON
    TEST_AFTER_INSTALL OFF
    TEST_COMMAND ${GMP_TEST_COMMAND}
)
