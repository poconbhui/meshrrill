include(ExternalProject)

set(
    MESHRRILL_COMMIT_ISH "master" CACHE STRING
    "The git commit-ish to checkout and build"
)

set(
    MESHRRILL_GIT_URL "${CMAKE_SOURCE_DIR}/.." CACHE STRING
    "The url of the git repo to checkout from. Default is one directory up."
)

set(MESHRRILL_PREFIX "${CMAKE_BINARY_DIR}/meshrrill" CACHE PATH "")

set(MESHRRILL_SOURCE_DIR  "${MESHRRILL_PREFIX}/src"  CACHE PATH "")
set(MESHRRILL_BUILD_DIR   "${MESHRRILL_PREFIX}/build"   CACHE PATH "")

set(MESHRRILL_INSTALL_DIR "${MESHRRILL_PREFIX}/install" CACHE PATH "")

set(
    MESHRRILL_CONFIG_EXTRA_OPTIONS "" CACHE STRING
    "Extra arguments to pass to the MEshRRILL configure command"
)

set(
    MESHRRILL_BUILD_EXTRA_OPTIONS "" CACHE STRING
    "Extra arguments to pass to the MEshRRILL build command"
)


if(MESHRRILL_COMMIT_ISH STREQUAL "current")
    set(MESHRRILL_SOURCE_DIR "${MESHRRILL_GIT_URL}")
else()
    set(MESHRRILL_GIT_OPTIONS
        GIT_REPOSITORY ${MESHRRILL_GIT_URL}
        GIT_TAG ${MESHRRILL_COMMIT_ISH}
    )
endif()

#
# Build MEshRRILL from the git repo
#

if(BUILD_SHARED_LIBS)
    set(
        MESHRRILL_LINKAGE
        "-DBUILD_SHARED_LIBS:BOOL=ON;"
        "-DBoost_USE_STATIC_LIBS:BOOL=OFF"
    )
else()
    set(
        MESHRRILL_LINKAGE
        "-DBUILD_SHARED_LIBS:BOOL=OFF;"
        "-DBoost_USE_STATIC_LIBS:BOOL=ON"
    )
endif()

ExternalProject_Add(
    meshrrill

    DEPENDS boost cgal libtiff

    PREFIX ${MESHRRILL_PREFIX}

    ${MESHRRILL_GIT_OPTIONS}

    SOURCE_DIR  ${MESHRRILL_SOURCE_DIR}
    BINARY_DIR  ${MESHRRILL_BUILD_DIR}
    INSTALL_DIR ${MESHRRILL_INSTALL_DIR}

    STAMP_DIR ${MESHRRILL_BUILD_DIR}

    CMAKE_ARGS
        "-DCMAKE_INSTALL_PREFIX:PATH=${MESHRRILL_INSTALL_DIR}"

        "-DBOOST_ROOT=${BOOST_INSTALL_DIR}"
        -DBoost_NO_BOOST_CMAKE:BOOL=ON
        -DBoost_USE_MULTITHREADED:BOOL=OFF

        -DMEshRRILL_ENABLE_TIFF_READER:BOOL=ON
        "-DCMAKE_PREFIX_PATH:PATH=${LIBTIFF_INSTALL_DIR}"

        "-DCGAL_DIR:PATH=${CGAL_INSTALL_DIR}/lib/CGAL"

        "-DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}"
        "-DCMAKE_VERBOSE_MAKEFILE:BOOL=${CMAKE_VERBOSE_MAKEFILE}"
        "-DCMAKE_C_FLAGS:STRING=${CMAKE_C_FLAGS} "
        "-DCMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS} "
        "-DCMAKE_C_COMPILER:STRING=${CMAKE_C_COMPILER}"
        "-DCMAKE_CXX_COMPILER:STRING=${CMAKE_CXX_COMPILER}"
        "-DCMAKE_EXE_LINKER_FLAGS:STRING=${CMAKE_EXE_LINKER_FLAGS} "
        "-DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS}"
        "-DCMAKE_INSTALL_RPATH:STRING=${CMAKE_INSTALL_RPATH}"

        ${MESHRRILL_LINKAGE}
        ${MESHRRILL_CONFIG_EXTRA_OPTIONS}

    BUILD_COMMAND
        ${CMAKE_COMMAND}
            --build "${MESHRRILL_BUILD_DIR}"
            ${MESHRRILL_BUILD_EXTRA_OPTIONS}
)
