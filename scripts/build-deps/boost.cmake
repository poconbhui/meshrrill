include(ExternalProject)


set(
    BOOST_URL "http://downloads.sourceforge.net/project/boost/boost/1.61.0/boost_1_61_0.tar.gz" CACHE STRING
    "The URL to get the boost tarball."
)
set(
    BOOST_MD5 "874805ba2e2ee415b1877ef3297bf8ad" CACHE STRING
    "MD5 hash of the boost tarball"
)

set(
    BOOST_TOOLSET "gcc" CACHE STRING
    "The toolset to use with Boost. eg. gcc, clang. This can be useful when the OSX gcc/clang toolset calls 'g++' when g++ isn't a clang wrapper."
)


set(BOOST_PREFIX      "${CMAKE_BINARY_DIR}/boost" CACHE PATH "")
set(BOOST_SOURCE_DIR  "${BOOST_PREFIX}/src"       CACHE PATH "")
set(BOOST_BUILD_DIR   "${BOOST_PREFIX}/build"     CACHE PATH "")
set(BOOST_INSTALL_DIR "${BOOST_PREFIX}"           CACHE PATH "")

set(
    BOOST_BUILD_EXTRA_OPTIONS "" CACHE STRING
    "Extra options to pass to the Boost build command"
)


if(CMAKE_VERBOSE_MAKEFILE)
    set(BOOST_VERBOSITY "-d+2")
else()
    set(BOOST_VERBOSITY "-d0")
endif()


if(BUILD_SHARED_LIBS)
    set(BOOST_LINKAGE "link=shared;runtime-link=shared")
else()
    set(BOOST_LINKAGE "link=static;runtime-link=static")
endif()

ExternalProject_Add(
    boost

    PREFIX ${BOOST_PREFIX}

    SOURCE_DIR ${BOOST_SOURCE_DIR}
    BINARY_DIR ${BOOST_BUILD_DIR}
    INSTALL_DIR ${BOOST_INSTALL_DIR}

    STAMP_DIR ${BOOST_BUILD_DIR}

    URL ${BOOST_URL}
    URL_MD5 ${BOOST_MD5}

    CONFIGURE_COMMAND
	${CMAKE_COMMAND} -E chdir ${BOOST_SOURCE_DIR}
            ./bootstrap.sh
                "--prefix=${BOOST_INSTALL_DIR}"
                "--with-toolset=${BOOST_TOOLSET}"
                --with-libraries=program_options,system,thread

    COMMAND
    # Set CXX as custom compiler for given toolset
    echo "using ${BOOST_TOOLSET} : custom : ${CMAKE_CXX_COMPILER} $<SEMICOLON>" >> "${BOOST_SOURCE_DIR}/project-config.jam"


    BUILD_COMMAND
	${CMAKE_COMMAND} -E chdir ${BOOST_SOURCE_DIR}
        ./b2
            ${BOOST_VERBOSITY}
            "--toolset=${BOOST_TOOLSET}-custom"
            "--prefix=${BOOST_INSTALL_DIR}"
            "--build-dir=${BOOST_BUILD_DIR}"
            "cxxflags=${CMAKE_CXX_FLAGS} "
            ${BOOST_LINKAGE}
            ${BOOST_BUILD_EXTRA_OPTIONS}

    INSTALL_COMMAND
	${CMAKE_COMMAND} -E chdir ${BOOST_SOURCE_DIR}
        ./b2
            ${BOOST_VERBOSITY}
            "--toolset=${BOOST_TOOLSET}-custom"
            "--prefix=${BOOST_INSTALL_DIR}"
            "--build-dir=${BOOST_BUILD_DIR}"
            ${BOOST_LINKAGE}
            install
)
