include(ExternalProject)


set(
    MPFR_URL "http://www.mpfr.org/mpfr-3.1.4/mpfr-3.1.4.tar.gz" CACHE STRING
    "URL to the MPFR tarball."
)
set(
    MPFR_MD5 "482ab3c120ffc959f631b4ba9ec59a46" CACHE STRING
    "MD5 hash of the MPFR tarball."
)


option(MPFR_ENABLE_TESTING "Test MPFR build before install" OFF)

if(MPFR_ENABLE_TESTING)
    set(MPFR_TEST_COMMAND "make check")
else()
    set(MPFR_TEST_COMMAND "true")
endif(MPFR_ENABLE_TESTING)


set(MPFR_PREFIX      "${CMAKE_BINARY_DIR}/mpfr" CACHE PATH "")
set(MPFR_SOURCE_DIR  "${MPFR_PREFIX}/src"       CACHE PATH "")
set(MPFR_BUILD_DIR   "${MPFR_PREFIX}/build"     CACHE PATH "")
set(MPFR_INSTALL_DIR "${MPFR_PREFIX}"           CACHE PATH "")

set(
    MPFR_BUILD_EXTRA_OPTIONS CACHE STRING
    "Extra options to pass to the MPFR build command"
)


if(CMAKE_VERBOSE_MAKEFILE)
    set(MPFR_VERBOSITY "--disable-silent-rules")
else()
    set(MPFR_VERBOSITY "--enable-silent-rules")
endif()

if(BUILD_SHARED_LIBS)
    set(MPFR_LINKAGE "--enable-shared;--disable-static")
else()
    set(MPFR_LINKAGE "--disable-shared;--enable-static")
endif()

ExternalProject_Add(
    mpfr

    DEPENDS gmp

    PREFIX ${MPFR_PREFIX}

    SOURCE_DIR  ${MPFR_SOURCE_DIR}
    BINARY_DIR  ${MPFR_BUILD_DIR}
    INSTALL_DIR ${MPFR_INSTALL_DIR}

    STAMP_DIR ${MPFR_BUILD_DIR}

    URL ${MPFR_URL}
    URL_MD5 ${MPFR_MD5}

    CONFIGURE_COMMAND
	${CMAKE_COMMAND} -E chdir ${MPFR_BUILD_DIR}
            ${MPFR_SOURCE_DIR}/configure
                ${MPFR_VERBOSITY}
                "CC=${CMAKE_C_COMPILER}"
                "CFLAGS=${CMAKE_C_FLAGS} "
                "CXX=${CMAKE_CXX_COMPILER}"
                "CXXFLAGS=${CMAKE_CXX_FLAGS} "
                "--prefix=${MPFR_INSTALL_DIR}"
                "--with-gmp=${GMP_INSTALL_DIR}"
                ${MPFR_LINKAGE}

    BUILD_COMMAND
        ${CMAKE_MAKE_PROGRAM}
            -C ${MPFR_BUILD_DIR}
            ${MPFR_BUILD_EXTRA_OPTIONS}

    TEST_BEFORE_INSTALL ON
    TEST_AFTER_INSTALL OFF
    TEST_COMMAND ${MPFR_TEST_COMMAND}
)
