#!/usr/bin/env bash

# Exit on error
set -o errexit
set -o nounset


# If there are arguments, the first should be a commit-ish, the rest get
# passed to CMake.
if [ $# -gt 0 ]; then
    meshrrill_commit_ish="$1"
    shift
else
    meshrrill_commit_ish=master
fi

pretty_commit_ish=meshrrill-$(echo $meshrrill_commit_ish | sed 's/^meshrrill-//')

source_tarball_name=${pretty_commit_ish}
binary_tarball_name=${pretty_commit_ish}-${DIST_ID}


source_dir="$(cd "$(dirname "$(which "${BASH_SOURCE[0]}")")"; echo $PWD)"
build_dir=$PWD/${pretty_commit_ish}/${DIST_ID}_build
tarball_dir=$PWD/${pretty_commit_ish}

meshrrill_git_url="$source_dir/../.."


cmake \
    -B$build_dir -H$source_dir \
    -DCMAKE_INSTALL_PREFIX:PATH="$tarball_dir" \
    -DMESHRRILL_COMMIT_ISH:STRING="$meshrrill_commit_ish" \
    -DMESHRRILL_GIT_URL:STRING="$meshrrill_git_url" \
    -DMESHRRILL_SOURCE_TARBALL_NAME:STRING="$source_tarball_name" \
    -DMESHRRILL_BINARY_TARBALL_NAME:STRING="$binary_tarball_name" \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -DCMAKE_BUILD_TYPE:STRING="Release" \
    -DCMAKE_EXE_LINKER_FLAGS:STRING="$CMAKE_EXE_LINKER_FLAGS" \
    -DBOOST_TOOLSET:STRING="$BOOST_TOOLSET" \
    -DCMAKE_C_COMPILER:PATH="$CMAKE_C_COMPILER" \
    -DCMAKE_C_FLAGS:PATH="$CMAKE_C_FLAGS" \
    -DCMAKE_CXX_COMPILER:PATH="$CMAKE_CXX_COMPILER" \
    -DCMAKE_CXX_FLAGS:STRING="$CMAKE_CXX_FLAGS" \
    $CMAKE_EXTRA_OPTIONS \
    "$@"

cmake --build $build_dir
